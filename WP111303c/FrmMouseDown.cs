﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP111303c
{
	public partial class FrmMouseDown : Form
	{
		private Point point;

		public FrmMouseDown()
		{
			InitializeComponent();
		}

		private void FrmMouseDown_MouseDown(object sender, MouseEventArgs e)
		{
			Graphics g = CreateGraphics();

			if (e.Button == MouseButtons.Right)
			{
				g.DrawEllipse(Pens.Red, 0, 0, 20, 20);
				g.DrawEllipse(Pens.Red, e.X, e.Y, 15, 15);
			}

			g.Dispose();

			point = e.Location;
		}

		private void FrmMouseDown_MouseMove(object sender, MouseEventArgs e)
		{
			Graphics g = CreateGraphics();

			if (e.Button == MouseButtons.Left)
			{
				g.DrawLine(Pens.Blue, point, e.Location);
			}

			g.Dispose();
		}
	}
}
