﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091101c
{
	class MinMax
	{
		static void Main(string[] args)
		{
			int max, min;

			int num1 = int.Parse(Console.ReadLine());
			int num2 = int.Parse(Console.ReadLine());

			if (num1 > num2)
			{
				max = num1;
				min = num2;
			}
			else
			{
				min = num1;
				max = num2;
			}
			Console.WriteLine("Max = " + max + ", Min = " + min);

			max = num1 > num2 ? num1 : num2;
			min = num1 < num2 ? num1 : num2;
			Console.WriteLine("Max = " + max + ", Min = " + min);
		}
	}
}
