﻿namespace WP110101c
{
	partial class 과제1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnTrans = new System.Windows.Forms.Button();
			this.lblName = new System.Windows.Forms.Label();
			this.lblAge = new System.Windows.Forms.Label();
			this.lblRName = new System.Windows.Forms.Label();
			this.lblRAge = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.txtAge = new System.Windows.Forms.TextBox();
			this.txtRAge = new System.Windows.Forms.TextBox();
			this.txtRName = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnTrans
			// 
			this.btnTrans.Location = new System.Drawing.Point(95, 116);
			this.btnTrans.Name = "btnTrans";
			this.btnTrans.Size = new System.Drawing.Size(85, 24);
			this.btnTrans.TabIndex = 0;
			this.btnTrans.Text = "전송하기";
			this.btnTrans.UseVisualStyleBackColor = true;
			this.btnTrans.Click += new System.EventHandler(this.BtnTrans_Click);
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(12, 38);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(29, 12);
			this.lblName.TabIndex = 1;
			this.lblName.Text = "이름";
			// 
			// lblAge
			// 
			this.lblAge.AutoSize = true;
			this.lblAge.Location = new System.Drawing.Point(12, 69);
			this.lblAge.Name = "lblAge";
			this.lblAge.Size = new System.Drawing.Size(29, 12);
			this.lblAge.TabIndex = 1;
			this.lblAge.Text = "나이";
			// 
			// lblRName
			// 
			this.lblRName.AutoSize = true;
			this.lblRName.Location = new System.Drawing.Point(12, 168);
			this.lblRName.Name = "lblRName";
			this.lblRName.Size = new System.Drawing.Size(69, 12);
			this.lblRName.TabIndex = 1;
			this.lblRName.Text = "입력한 이름";
			// 
			// lblRAge
			// 
			this.lblRAge.AutoSize = true;
			this.lblRAge.Location = new System.Drawing.Point(12, 206);
			this.lblRAge.Name = "lblRAge";
			this.lblRAge.Size = new System.Drawing.Size(69, 12);
			this.lblRAge.TabIndex = 1;
			this.lblRAge.Text = "입력한 나이";
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(95, 35);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(121, 21);
			this.txtName.TabIndex = 2;
			// 
			// txtAge
			// 
			this.txtAge.Location = new System.Drawing.Point(95, 66);
			this.txtAge.Name = "txtAge";
			this.txtAge.Size = new System.Drawing.Size(121, 21);
			this.txtAge.TabIndex = 2;
			// 
			// txtRAge
			// 
			this.txtRAge.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.txtRAge.ForeColor = System.Drawing.Color.Blue;
			this.txtRAge.Location = new System.Drawing.Point(94, 203);
			this.txtRAge.Name = "txtRAge";
			this.txtRAge.ReadOnly = true;
			this.txtRAge.Size = new System.Drawing.Size(121, 21);
			this.txtRAge.TabIndex = 2;
			// 
			// txtRName
			// 
			this.txtRName.BackColor = System.Drawing.SystemColors.ButtonShadow;
			this.txtRName.ForeColor = System.Drawing.Color.Red;
			this.txtRName.Location = new System.Drawing.Point(94, 165);
			this.txtRName.Name = "txtRName";
			this.txtRName.ReadOnly = true;
			this.txtRName.Size = new System.Drawing.Size(121, 21);
			this.txtRName.TabIndex = 2;
			// 
			// 과제1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.txtRAge);
			this.Controls.Add(this.txtRName);
			this.Controls.Add(this.txtAge);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.lblRAge);
			this.Controls.Add(this.lblRName);
			this.Controls.Add(this.lblAge);
			this.Controls.Add(this.lblName);
			this.Controls.Add(this.btnTrans);
			this.Name = "과제1";
			this.Text = "과제1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnTrans;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Label lblAge;
		private System.Windows.Forms.Label lblRName;
		private System.Windows.Forms.Label lblRAge;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.TextBox txtAge;
		private System.Windows.Forms.TextBox txtRAge;
		private System.Windows.Forms.TextBox txtRName;
	}
}

