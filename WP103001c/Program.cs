﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP103001c
{
	class Program
	{
		static void Main(string[] args)
		{
			Student student = new Student();
			Man man = student;
			student.Say();
			man.Say();
			object @object = new object();
			IStudent istud = student;
			student.Learn();
			istud.Learn();
		}
	}

	class Man
	{
		public virtual void Say()
		{
			Console.WriteLine("Ahh!");
		}
	}

	class Student : Man, IStudent
	{
		public override void Say()
		{
			Console.WriteLine("I'm a student.");
		}

		public void Learn()
		{
			Console.WriteLine("Studying");
		}
	}

	interface IStudent
	{
		void Learn();
	}
}
