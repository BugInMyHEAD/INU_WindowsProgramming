﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP090602h
{
	public class Program
	{
		static void Main(string[] args)
		{
			GuessingGame.Main(null);
		}
	}

	class GuessingGame
	{
		private static Random random = new Random();

		public static void Main(string[] args)
		{
			new GuessingGame(1, 100).Play();
		}

		private int min, max;

		public GuessingGame(int min, int max)
		{
			if (min > max)
			{
				throw new ArgumentException("min > max");
			}

			if (min < 0)
			{
				throw new ArgumentException("min < 0");
			}

			this.min = min;
			this.max = max;
		}

		public void Play()
		{
			int a_su = random.Next(min, max);

			{
				int n1 = 1;
				for (; ; n1++)
				{
					Console.Write(min + "에서 " + max + " 사이 상대가 생각하고 있는 수를 입력하세요:  ");
					int su = Convert.ToInt16(Console.ReadLine());

					if (su > a_su)
					{
						Console.WriteLine("너무 커");
					}
					else if (su < a_su)
					{
						Console.WriteLine("너무 작아");
					}
					else // when su == a_su
					{
						Console.WriteLine("정답");
						Console.WriteLine("시도 횟수 =" + n1);

						break;
					}
				}
			}
		}
	}
}
