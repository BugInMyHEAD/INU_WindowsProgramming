﻿namespace WP112201c
{
	partial class FrmDyanmicForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOk = new System.Windows.Forms.Button();
			this.grp1 = new System.Windows.Forms.GroupBox();
			this.lbl1 = new System.Windows.Forms.Label();
			this.radMale = new System.Windows.Forms.RadioButton();
			this.grp1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOk
			// 
			this.btnOk.Location = new System.Drawing.Point(76, 165);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 0;
			this.btnOk.Text = "확인";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
			// 
			// grp1
			// 
			this.grp1.Controls.Add(this.radMale);
			this.grp1.Controls.Add(this.lbl1);
			this.grp1.Location = new System.Drawing.Point(37, 27);
			this.grp1.Name = "grp1";
			this.grp1.Size = new System.Drawing.Size(200, 107);
			this.grp1.TabIndex = 1;
			this.grp1.TabStop = false;
			this.grp1.Text = "groupBox1";
			// 
			// lbl1
			// 
			this.lbl1.AutoSize = true;
			this.lbl1.Location = new System.Drawing.Point(61, 21);
			this.lbl1.Name = "lbl1";
			this.lbl1.Size = new System.Drawing.Size(38, 12);
			this.lbl1.TabIndex = 0;
			this.lbl1.Text = "label1";
			// 
			// radMale
			// 
			this.radMale.AutoSize = true;
			this.radMale.Location = new System.Drawing.Point(63, 61);
			this.radMale.Name = "radMale";
			this.radMale.Size = new System.Drawing.Size(47, 16);
			this.radMale.TabIndex = 1;
			this.radMale.TabStop = true;
			this.radMale.Text = "남자";
			this.radMale.UseVisualStyleBackColor = true;
			// 
			// FrmDyanmicForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.grp1);
			this.Controls.Add(this.btnOk);
			this.Name = "FrmDyanmicForm";
			this.Text = "객체동적생성";
			this.grp1.ResumeLayout(false);
			this.grp1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.GroupBox grp1;
		private System.Windows.Forms.RadioButton radMale;
		private System.Windows.Forms.Label lbl1;
	}
}

