﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091302h
{
	public class SRPGame
	{
		public static int SRPSize { get; } = typeof(SRP).GetEnumValues().Length;

		static Random random = new Random();
		
		static void Main(string[] args)
		{
			var computer = new Player();
			var player = new Player();

			var srpGame = new SRPGame(computer, player);
			
			for (; srpGame.HasNextMatch(); srpGame.MatchQueue.Enqueue(new Match(computer, player)))
			{
				int intFromUser;
				for (; ; )
				{
					Console.Write("1, 2, 3 중 하나의 숫자 입력 : ");
					try
					{
						intFromUser = int.Parse(Console.ReadLine());

						switch (intFromUser)
						{
						case -1:
						case 1:
						case 2:
						case 3:
							goto __successfulInput_intFromUser;
						default:
							throw new FormatException();
						}
					}
					catch (FormatException) { }
				}

__successfulInput_intFromUser:
				if (-1 == intFromUser)
				{
					break;
				}

				computer.RandomInMind();
				player.InMind = (SRP)intFromUser;

				var mr = SimulateMatch(player.InMind, computer.InMind);
				if (mr < 0)
				{
					Console.WriteLine("내가 패");
				}
				else if (mr > 0)
				{
					Console.WriteLine("내가 승");
				}
				else
				{
					Console.WriteLine("서로 비김");
				}
				Console.WriteLine("컴의 선택은 " + (int)computer.InMind);

				srpGame.NextMatch();

				Console.WriteLine("------------------------------------");
			}

			Console.WriteLine("\n총 게임 횟수 " + srpGame.Round + " 번");
			Console.WriteLine("컴의 가위 = {0}, 컴의 바위 = {1}, 컴의 보 = {2}", computer[SRP.S], computer[SRP.R], computer[SRP.P]);
			var playerVsComputerScore = srpGame.ScoreBoard[player][computer];
			Console.WriteLine("서로 비김 = {0}, 내가 승 = {1}, 내가 패 = {2}", playerVsComputerScore.Draw, playerVsComputerScore.Win, playerVsComputerScore.Loss);
		}

		public static int SimulateMatch(SRP a, SRP b)
		{
			if (SRP.X == a || SRP.X == b) throw new InvalidOperationException();

			if (a == b) return 0;

			// The cases 'a' loses.
			if (SRP.S == a && SRP.R == b || SRP.R == a && SRP.P == b || SRP.P == a && SRP.S == b) return -1;

			return 1;
		}

		[Obsolete]
		public Queue<Match> MatchQueue = new Queue<Match>();

		public int Round { get; private set; }
		public Dictionary<Player, Dictionary<Player, Score>> ScoreBoard { get; } = new Dictionary<Player, Dictionary<Player, Score>>();

		public SRPGame(params Player[] players)
		{
			for (int i2 = 0; i2 < players.Length; i2++)
			{
				for (int j2 = i2 + 1; j2 < players.Length; j2++)
				{
					Match match = new Match(players[i2], players[j2]);
					MatchQueue.Enqueue(match);
					InitScoreBoard(match);
				}
			}
		}

		public bool HasNextMatch()
		{
			return 0 != MatchQueue.Count;
		}

		[Obsolete]
		public int NextMatch()
		{
			return NextMatch(MatchQueue.Dequeue());
		}

		// This method records match results. If you just want a result but not record, use simulateMatch(...).
		public int NextMatch(Match match)
		{
			//InitScoreBoard(match);

			var retVal = SimulateMatch(match.A.Reveal(), match.B.Reveal());
			if (retVal < 0)
			{
				ScoreBoard[match.A][match.B].Lost();
				ScoreBoard[match.B][match.A].Won();
			}
			else if (retVal > 0)
			{
				ScoreBoard[match.A][match.B].Won();
				ScoreBoard[match.B][match.A].Lost();
			}
			else
			{
				ScoreBoard[match.A][match.B].Drew();
				ScoreBoard[match.B][match.A].Drew();
			}

			if (!HasNextMatch()) Round++;

			return retVal;
		}

		private void InitScoreBoard(Match match)
		{
			if (!ScoreBoard.ContainsKey(match.A)) ScoreBoard[match.A] = new Dictionary<Player, Score>();
			if (!ScoreBoard.ContainsKey(match.B)) ScoreBoard[match.B] = new Dictionary<Player, Score>();

			if (!ScoreBoard[match.A].ContainsKey(match.B)) ScoreBoard[match.A][match.B] = new Score();
			if (!ScoreBoard[match.B].ContainsKey(match.A)) ScoreBoard[match.B][match.A] = new Score();
		}

		public class Player
		{
			public SRP InMind { get; set; }
			int[] srpCount = new int[SRPSize];
			public int this[SRP srp] { get => srpCount[(int)srp]; set => srpCount[(int)srp] = value; }

			public void ClearCount()
			{
				for(int i2 = 0; i2 < srpCount.Length; i2++)
				{
					srpCount[i2] = 0;
				}
			}

			internal SRP Reveal()
			{
				switch (InMind)
				{
				case SRP.X:
					throw new InvalidOperationException();
				case SRP.S:
				case SRP.R:
				case SRP.P:
					this[InMind]++;
					break;
				default:
					throw new ApplicationException();
				}

				var retVal = InMind;
				InMind = SRP.X;

				return retVal;
			}

			public SRP RandomInMind()
			{
				return InMind = (SRP)random.Next(1, SRPSize);
			}

			public SRP ForeseeInMind(SRP opponent, double winRate, double drawRate, double loseRate)
			{
				if (winRate < 0 || drawRate < 0 || loseRate < 0)
				{
					throw new ArgumentOutOfRangeException("All 'rates' must be nonnegative");
				}

				double total = winRate + drawRate + loseRate;
				double drawThreshold = loseRate;
				double winThreshold = drawThreshold + drawRate;
				double randomDouble = random.NextDouble() * total;
				if (randomDouble >= winThreshold)
				{
					InMind = Counter(opponent);
				}
				else if (randomDouble >= drawThreshold)
				{
					InMind = opponent;
				}
				else
				{
					InMind = Counter(Counter(opponent));
				}

				return InMind;
			}

			public SRP Counter(SRP opponent)
			{
				switch (opponent)
				{
					case SRP.S:
						return SRP.R;
					case SRP.R:
						return SRP.P;
					case SRP.P:
						return SRP.S;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public enum SRP
		{
			X, S, R, P
		}

		public class Score
		{
			public int Win { get; private set; }
			public int Loss { get; private set; }
			public int Draw { get; private set; }
			public int Match => Win + Loss + Draw;

			public int Won() => ++Win;
			public int Lost() => ++Loss;
			public int Drew() => ++Draw;
		}

		public class Match
		{
			public Player A { get; }
			public Player B { get; }

			public Match(Player a, Player b)
			{
				A = a;
				B = b;
			}
		}
	}
}
