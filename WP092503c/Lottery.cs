﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP092503c
{
	public static class Lottery
	{
		static Random random = new Random();

		static void Main(string[] args)
		{
			Console.WriteLine("로또 번호 생성기");
			int[] arr = Pick(6, 1, 45 + 1);

			foreach (var var2 in arr)
			{
				Console.Write($"{var2,4}");
			}
			Console.WriteLine();
		}

		public static int[] Pick(int num, int min, int max)
		{
			if (max - min < num)
				throw new ArgumentOutOfRangeException();

			var retVal = new int[num];

			for (var i2 = 0; i2 < retVal.Length; )
			{
				retVal[i2] = random.Next(min, max);
				int j2;
				for(j2 = 0; j2 < i2; j2++)
				{
					if(retVal[i2] == retVal[j2])
					{
						break;
					}
				}
				if (j2 >= i2)
				{
					i2++;
				}
			}

			return retVal;
		}
	}
}
