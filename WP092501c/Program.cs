﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP092003c
{
	class Program
	{
		static void Main(string[] args)
		{
			int[] arr = new int[] { 1, 3, 5, 7, 9, 2, 4, 9, 8, 7 };

			Console.Write("1부터 10까지 찾고자 하는 수 입력 : ");
			int intFromUser = int.Parse(Console.ReadLine());
			for (var i2 = 0; i2 < arr.Length; i2++)
			{
				if (intFromUser == arr[i2])
					Console.WriteLine($"찾는 데이터 {intFromUser}은 {i2+1}번째 방에 있음");
			}
		}
	}
}
