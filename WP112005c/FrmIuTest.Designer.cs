﻿namespace WP112005c
{
	partial class FrmIuTest
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.grp1 = new System.Windows.Forms.GroupBox();
			this.rad1_Gwangju = new System.Windows.Forms.RadioButton();
			this.rad1_Busan = new System.Windows.Forms.RadioButton();
			this.rad1_Incheon = new System.Windows.Forms.RadioButton();
			this.rad1_Seoul = new System.Windows.Forms.RadioButton();
			this.grp2 = new System.Windows.Forms.GroupBox();
			this.chk2_cheongju = new System.Windows.Forms.CheckBox();
			this.chk2_Seoul = new System.Windows.Forms.CheckBox();
			this.chk2_Songdo = new System.Windows.Forms.CheckBox();
			this.chk2_Incheon = new System.Windows.Forms.CheckBox();
			this.grp3 = new System.Windows.Forms.GroupBox();
			this.rad3_gyeyang = new System.Windows.Forms.RadioButton();
			this.rad3_jiri = new System.Windows.Forms.RadioButton();
			this.rad3_halla = new System.Windows.Forms.RadioButton();
			this.rad3_baekdu = new System.Windows.Forms.RadioButton();
			this.grp4 = new System.Windows.Forms.GroupBox();
			this.rad4_4 = new System.Windows.Forms.RadioButton();
			this.rad4_3 = new System.Windows.Forms.RadioButton();
			this.rad4_2 = new System.Windows.Forms.RadioButton();
			this.rad4_1 = new System.Windows.Forms.RadioButton();
			this.grp5 = new System.Windows.Forms.GroupBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.rad5_iu = new System.Windows.Forms.RadioButton();
			this.rad5_ivy = new System.Windows.Forms.RadioButton();
			this.rad5_iq = new System.Windows.Forms.RadioButton();
			this.rad5_idol = new System.Windows.Forms.RadioButton();
			this.btnGrading = new System.Windows.Forms.Button();
			this.lblScoreStr = new System.Windows.Forms.Label();
			this.lblScoreFig = new System.Windows.Forms.Label();
			this.grp1.SuspendLayout();
			this.grp2.SuspendLayout();
			this.grp3.SuspendLayout();
			this.grp4.SuspendLayout();
			this.grp5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// grp1
			// 
			this.grp1.Controls.Add(this.rad1_Gwangju);
			this.grp1.Controls.Add(this.rad1_Busan);
			this.grp1.Controls.Add(this.rad1_Incheon);
			this.grp1.Controls.Add(this.rad1_Seoul);
			this.grp1.Location = new System.Drawing.Point(12, 12);
			this.grp1.Name = "grp1";
			this.grp1.Size = new System.Drawing.Size(387, 48);
			this.grp1.TabIndex = 0;
			this.grp1.TabStop = false;
			this.grp1.Text = "1. 우리나라 수도는?";
			// 
			// rad1_Gwangju
			// 
			this.rad1_Gwangju.AutoSize = true;
			this.rad1_Gwangju.Location = new System.Drawing.Point(218, 20);
			this.rad1_Gwangju.Name = "rad1_Gwangju";
			this.rad1_Gwangju.Size = new System.Drawing.Size(47, 16);
			this.rad1_Gwangju.TabIndex = 0;
			this.rad1_Gwangju.TabStop = true;
			this.rad1_Gwangju.Text = "광주";
			this.rad1_Gwangju.UseVisualStyleBackColor = true;
			// 
			// rad1_Busan
			// 
			this.rad1_Busan.AutoSize = true;
			this.rad1_Busan.Location = new System.Drawing.Point(154, 20);
			this.rad1_Busan.Name = "rad1_Busan";
			this.rad1_Busan.Size = new System.Drawing.Size(47, 16);
			this.rad1_Busan.TabIndex = 0;
			this.rad1_Busan.TabStop = true;
			this.rad1_Busan.Text = "부산";
			this.rad1_Busan.UseVisualStyleBackColor = true;
			// 
			// rad1_Incheon
			// 
			this.rad1_Incheon.AutoSize = true;
			this.rad1_Incheon.Location = new System.Drawing.Point(91, 20);
			this.rad1_Incheon.Name = "rad1_Incheon";
			this.rad1_Incheon.Size = new System.Drawing.Size(47, 16);
			this.rad1_Incheon.TabIndex = 0;
			this.rad1_Incheon.TabStop = true;
			this.rad1_Incheon.Text = "인천";
			this.rad1_Incheon.UseVisualStyleBackColor = true;
			// 
			// rad1_Seoul
			// 
			this.rad1_Seoul.AutoSize = true;
			this.rad1_Seoul.Location = new System.Drawing.Point(25, 20);
			this.rad1_Seoul.Name = "rad1_Seoul";
			this.rad1_Seoul.Size = new System.Drawing.Size(47, 16);
			this.rad1_Seoul.TabIndex = 0;
			this.rad1_Seoul.TabStop = true;
			this.rad1_Seoul.Text = "서울";
			this.rad1_Seoul.UseVisualStyleBackColor = true;
			// 
			// grp2
			// 
			this.grp2.Controls.Add(this.chk2_cheongju);
			this.grp2.Controls.Add(this.chk2_Seoul);
			this.grp2.Controls.Add(this.chk2_Songdo);
			this.grp2.Controls.Add(this.chk2_Incheon);
			this.grp2.Location = new System.Drawing.Point(12, 66);
			this.grp2.Name = "grp2";
			this.grp2.Size = new System.Drawing.Size(387, 48);
			this.grp2.TabIndex = 0;
			this.grp2.TabStop = false;
			this.grp2.Text = "2. 인천대학교 위치는?";
			// 
			// chk2_cheongju
			// 
			this.chk2_cheongju.AutoSize = true;
			this.chk2_cheongju.Location = new System.Drawing.Point(217, 21);
			this.chk2_cheongju.Name = "chk2_cheongju";
			this.chk2_cheongju.Size = new System.Drawing.Size(48, 16);
			this.chk2_cheongju.TabIndex = 0;
			this.chk2_cheongju.Text = "청주";
			this.chk2_cheongju.UseVisualStyleBackColor = true;
			// 
			// chk2_Seoul
			// 
			this.chk2_Seoul.AutoSize = true;
			this.chk2_Seoul.Location = new System.Drawing.Point(154, 21);
			this.chk2_Seoul.Name = "chk2_Seoul";
			this.chk2_Seoul.Size = new System.Drawing.Size(48, 16);
			this.chk2_Seoul.TabIndex = 0;
			this.chk2_Seoul.Text = "서울";
			this.chk2_Seoul.UseVisualStyleBackColor = true;
			// 
			// chk2_Songdo
			// 
			this.chk2_Songdo.AutoSize = true;
			this.chk2_Songdo.Location = new System.Drawing.Point(90, 21);
			this.chk2_Songdo.Name = "chk2_Songdo";
			this.chk2_Songdo.Size = new System.Drawing.Size(48, 16);
			this.chk2_Songdo.TabIndex = 0;
			this.chk2_Songdo.Text = "송도";
			this.chk2_Songdo.UseVisualStyleBackColor = true;
			// 
			// chk2_Incheon
			// 
			this.chk2_Incheon.AutoSize = true;
			this.chk2_Incheon.Location = new System.Drawing.Point(25, 21);
			this.chk2_Incheon.Name = "chk2_Incheon";
			this.chk2_Incheon.Size = new System.Drawing.Size(48, 16);
			this.chk2_Incheon.TabIndex = 0;
			this.chk2_Incheon.Text = "인천";
			this.chk2_Incheon.UseVisualStyleBackColor = true;
			// 
			// grp3
			// 
			this.grp3.Controls.Add(this.rad3_gyeyang);
			this.grp3.Controls.Add(this.rad3_jiri);
			this.grp3.Controls.Add(this.rad3_halla);
			this.grp3.Controls.Add(this.rad3_baekdu);
			this.grp3.Location = new System.Drawing.Point(12, 120);
			this.grp3.Name = "grp3";
			this.grp3.Size = new System.Drawing.Size(387, 48);
			this.grp3.TabIndex = 0;
			this.grp3.TabStop = false;
			this.grp3.Text = "3. 인천의 산은?";
			// 
			// rad3_gyeyang
			// 
			this.rad3_gyeyang.AutoSize = true;
			this.rad3_gyeyang.Location = new System.Drawing.Point(218, 20);
			this.rad3_gyeyang.Name = "rad3_gyeyang";
			this.rad3_gyeyang.Size = new System.Drawing.Size(59, 16);
			this.rad3_gyeyang.TabIndex = 0;
			this.rad3_gyeyang.TabStop = true;
			this.rad3_gyeyang.Text = "계양산";
			this.rad3_gyeyang.UseVisualStyleBackColor = true;
			// 
			// rad3_jiri
			// 
			this.rad3_jiri.AutoSize = true;
			this.rad3_jiri.Location = new System.Drawing.Point(154, 20);
			this.rad3_jiri.Name = "rad3_jiri";
			this.rad3_jiri.Size = new System.Drawing.Size(59, 16);
			this.rad3_jiri.TabIndex = 0;
			this.rad3_jiri.TabStop = true;
			this.rad3_jiri.Text = "지리산";
			this.rad3_jiri.UseVisualStyleBackColor = true;
			// 
			// rad3_halla
			// 
			this.rad3_halla.AutoSize = true;
			this.rad3_halla.Location = new System.Drawing.Point(91, 20);
			this.rad3_halla.Name = "rad3_halla";
			this.rad3_halla.Size = new System.Drawing.Size(59, 16);
			this.rad3_halla.TabIndex = 0;
			this.rad3_halla.TabStop = true;
			this.rad3_halla.Text = "한라산";
			this.rad3_halla.UseVisualStyleBackColor = true;
			// 
			// rad3_baekdu
			// 
			this.rad3_baekdu.AutoSize = true;
			this.rad3_baekdu.Location = new System.Drawing.Point(25, 20);
			this.rad3_baekdu.Name = "rad3_baekdu";
			this.rad3_baekdu.Size = new System.Drawing.Size(59, 16);
			this.rad3_baekdu.TabIndex = 0;
			this.rad3_baekdu.TabStop = true;
			this.rad3_baekdu.Text = "백두산";
			this.rad3_baekdu.UseVisualStyleBackColor = true;
			// 
			// grp4
			// 
			this.grp4.Controls.Add(this.rad4_4);
			this.grp4.Controls.Add(this.rad4_3);
			this.grp4.Controls.Add(this.rad4_2);
			this.grp4.Controls.Add(this.rad4_1);
			this.grp4.Location = new System.Drawing.Point(12, 174);
			this.grp4.Name = "grp4";
			this.grp4.Size = new System.Drawing.Size(387, 316);
			this.grp4.TabIndex = 0;
			this.grp4.TabStop = false;
			this.grp4.Text = "4. 다음 중에서 아이유가 아닌 것은?";
			// 
			// rad4_4
			// 
			this.rad4_4.AutoSize = true;
			this.rad4_4.Image = global::WP112005c.Properties.Resources.Pic4_4;
			this.rad4_4.Location = new System.Drawing.Point(187, 175);
			this.rad4_4.Name = "rad4_4";
			this.rad4_4.Size = new System.Drawing.Size(137, 126);
			this.rad4_4.TabIndex = 0;
			this.rad4_4.TabStop = true;
			this.rad4_4.UseVisualStyleBackColor = true;
			// 
			// rad4_3
			// 
			this.rad4_3.AutoSize = true;
			this.rad4_3.Image = global::WP112005c.Properties.Resources.Pic4_3;
			this.rad4_3.Location = new System.Drawing.Point(25, 178);
			this.rad4_3.Name = "rad4_3";
			this.rad4_3.Size = new System.Drawing.Size(143, 121);
			this.rad4_3.TabIndex = 0;
			this.rad4_3.TabStop = true;
			this.rad4_3.UseVisualStyleBackColor = true;
			// 
			// rad4_2
			// 
			this.rad4_2.AutoSize = true;
			this.rad4_2.Image = global::WP112005c.Properties.Resources.Pic4_2;
			this.rad4_2.Location = new System.Drawing.Point(187, 21);
			this.rad4_2.Name = "rad4_2";
			this.rad4_2.Size = new System.Drawing.Size(137, 139);
			this.rad4_2.TabIndex = 0;
			this.rad4_2.TabStop = true;
			this.rad4_2.UseVisualStyleBackColor = true;
			// 
			// rad4_1
			// 
			this.rad4_1.AutoSize = true;
			this.rad4_1.Image = global::WP112005c.Properties.Resources.Pic4_1;
			this.rad4_1.Location = new System.Drawing.Point(25, 20);
			this.rad4_1.Name = "rad4_1";
			this.rad4_1.Size = new System.Drawing.Size(145, 141);
			this.rad4_1.TabIndex = 0;
			this.rad4_1.TabStop = true;
			this.rad4_1.UseVisualStyleBackColor = true;
			// 
			// grp5
			// 
			this.grp5.Controls.Add(this.pictureBox1);
			this.grp5.Controls.Add(this.rad5_iu);
			this.grp5.Controls.Add(this.rad5_ivy);
			this.grp5.Controls.Add(this.rad5_iq);
			this.grp5.Controls.Add(this.rad5_idol);
			this.grp5.Location = new System.Drawing.Point(12, 496);
			this.grp5.Name = "grp5";
			this.grp5.Size = new System.Drawing.Size(387, 215);
			this.grp5.TabIndex = 0;
			this.grp5.TabStop = false;
			this.grp5.Text = "5. 아래 사진은 누구인가?";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::WP112005c.Properties.Resources.Pic5_5;
			this.pictureBox1.Location = new System.Drawing.Point(15, 53);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(135, 143);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// rad5_iu
			// 
			this.rad5_iu.AutoSize = true;
			this.rad5_iu.Location = new System.Drawing.Point(198, 119);
			this.rad5_iu.Name = "rad5_iu";
			this.rad5_iu.Size = new System.Drawing.Size(59, 16);
			this.rad5_iu.TabIndex = 0;
			this.rad5_iu.TabStop = true;
			this.rad5_iu.Text = "아이유";
			this.rad5_iu.UseVisualStyleBackColor = true;
			// 
			// rad5_ivy
			// 
			this.rad5_ivy.AutoSize = true;
			this.rad5_ivy.Location = new System.Drawing.Point(198, 97);
			this.rad5_ivy.Name = "rad5_ivy";
			this.rad5_ivy.Size = new System.Drawing.Size(59, 16);
			this.rad5_ivy.TabIndex = 0;
			this.rad5_ivy.TabStop = true;
			this.rad5_ivy.Text = "아이비";
			this.rad5_ivy.UseVisualStyleBackColor = true;
			// 
			// rad5_iq
			// 
			this.rad5_iq.AutoSize = true;
			this.rad5_iq.Location = new System.Drawing.Point(198, 75);
			this.rad5_iq.Name = "rad5_iq";
			this.rad5_iq.Size = new System.Drawing.Size(59, 16);
			this.rad5_iq.TabIndex = 0;
			this.rad5_iq.TabStop = true;
			this.rad5_iq.Text = "아이큐";
			this.rad5_iq.UseVisualStyleBackColor = true;
			// 
			// rad5_idol
			// 
			this.rad5_idol.AutoSize = true;
			this.rad5_idol.Location = new System.Drawing.Point(198, 53);
			this.rad5_idol.Name = "rad5_idol";
			this.rad5_idol.Size = new System.Drawing.Size(59, 16);
			this.rad5_idol.TabIndex = 0;
			this.rad5_idol.TabStop = true;
			this.rad5_idol.Text = "아이돌";
			this.rad5_idol.UseVisualStyleBackColor = true;
			// 
			// btnGrading
			// 
			this.btnGrading.Location = new System.Drawing.Point(75, 728);
			this.btnGrading.Name = "btnGrading";
			this.btnGrading.Size = new System.Drawing.Size(75, 23);
			this.btnGrading.TabIndex = 1;
			this.btnGrading.Text = "채점하기";
			this.btnGrading.UseVisualStyleBackColor = true;
			this.btnGrading.Click += new System.EventHandler(this.BtnGrading_Click);
			// 
			// lblScoreStr
			// 
			this.lblScoreStr.AutoSize = true;
			this.lblScoreStr.Location = new System.Drawing.Point(242, 733);
			this.lblScoreStr.Name = "lblScoreStr";
			this.lblScoreStr.Size = new System.Drawing.Size(41, 12);
			this.lblScoreStr.TabIndex = 2;
			this.lblScoreStr.Text = "점수 : ";
			// 
			// lblScoreFig
			// 
			this.lblScoreFig.AutoSize = true;
			this.lblScoreFig.Location = new System.Drawing.Point(314, 733);
			this.lblScoreFig.Name = "lblScoreFig";
			this.lblScoreFig.Size = new System.Drawing.Size(9, 12);
			this.lblScoreFig.TabIndex = 2;
			this.lblScoreFig.Text = " ";
			// 
			// FrmTest
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(413, 768);
			this.Controls.Add(this.lblScoreFig);
			this.Controls.Add(this.lblScoreStr);
			this.Controls.Add(this.btnGrading);
			this.Controls.Add(this.grp2);
			this.Controls.Add(this.grp4);
			this.Controls.Add(this.grp5);
			this.Controls.Add(this.grp3);
			this.Controls.Add(this.grp1);
			this.Name = "FrmTest";
			this.Text = "시험문제";
			this.grp1.ResumeLayout(false);
			this.grp1.PerformLayout();
			this.grp2.ResumeLayout(false);
			this.grp2.PerformLayout();
			this.grp3.ResumeLayout(false);
			this.grp3.PerformLayout();
			this.grp4.ResumeLayout(false);
			this.grp4.PerformLayout();
			this.grp5.ResumeLayout(false);
			this.grp5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox grp1;
		private System.Windows.Forms.RadioButton rad1_Gwangju;
		private System.Windows.Forms.RadioButton rad1_Busan;
		private System.Windows.Forms.RadioButton rad1_Incheon;
		private System.Windows.Forms.RadioButton rad1_Seoul;
		private System.Windows.Forms.GroupBox grp2;
		private System.Windows.Forms.CheckBox chk2_cheongju;
		private System.Windows.Forms.CheckBox chk2_Seoul;
		private System.Windows.Forms.CheckBox chk2_Songdo;
		private System.Windows.Forms.CheckBox chk2_Incheon;
		private System.Windows.Forms.GroupBox grp3;
		private System.Windows.Forms.RadioButton rad3_gyeyang;
		private System.Windows.Forms.RadioButton rad3_jiri;
		private System.Windows.Forms.RadioButton rad3_halla;
		private System.Windows.Forms.RadioButton rad3_baekdu;
		private System.Windows.Forms.GroupBox grp4;
		private System.Windows.Forms.RadioButton rad4_4;
		private System.Windows.Forms.RadioButton rad4_2;
		private System.Windows.Forms.RadioButton rad4_1;
		private System.Windows.Forms.GroupBox grp5;
		private System.Windows.Forms.RadioButton rad5_iu;
		private System.Windows.Forms.RadioButton rad5_ivy;
		private System.Windows.Forms.RadioButton rad5_iq;
		private System.Windows.Forms.RadioButton rad5_idol;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.RadioButton rad4_3;
		private System.Windows.Forms.Button btnGrading;
		private System.Windows.Forms.Label lblScoreStr;
		private System.Windows.Forms.Label lblScoreFig;
	}
}

