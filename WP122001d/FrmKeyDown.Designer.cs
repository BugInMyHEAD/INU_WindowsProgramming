﻿namespace WP112001d
{
	partial class FrmKeyDown
	{

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>


		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.lblPointLable = new System.Windows.Forms.Label();
			this.lblPointCounter = new System.Windows.Forms.Label();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// lblPointLable
			// 
			this.lblPointLable.AutoSize = true;
			this.lblPointLable.Location = new System.Drawing.Point(11, 261);
			this.lblPointLable.Name = "lblPointLable";
			this.lblPointLable.Size = new System.Drawing.Size(29, 12);
			this.lblPointLable.TabIndex = 0;
			this.lblPointLable.Text = "점수";
			// 
			// lblPointCounter
			// 
			this.lblPointCounter.AutoSize = true;
			this.lblPointCounter.Location = new System.Drawing.Point(49, 261);
			this.lblPointCounter.Name = "lblPointCounter";
			this.lblPointCounter.Size = new System.Drawing.Size(9, 12);
			this.lblPointCounter.TabIndex = 0;
			this.lblPointCounter.Text = " ";
			// 
			// timer
			// 
			this.timer.Tick += new System.EventHandler(this.Timer_Tick);
			// 
			// FrmKeyDown
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(334, 282);
			this.Controls.Add(this.lblPointCounter);
			this.Controls.Add(this.lblPointLable);
			this.Name = "FrmKeyDown";
			this.Text = "FrmKeyDown";
			this.SizeChanged += new System.EventHandler(this.FrmKeyDown_SizeChanged);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmKeyDown_Paint);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Timer_Tick_Paint);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmKeyDown_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		#endregion

		private System.Windows.Forms.Label lblPointLable;
		private System.Windows.Forms.Label lblPointCounter;
		private System.Windows.Forms.Timer timer;
	}
}

