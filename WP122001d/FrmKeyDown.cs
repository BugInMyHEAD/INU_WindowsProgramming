﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP112001d
{
	public partial class FrmKeyDown : Form
	{
		Random random = new Random();
		Ball player;
		Ball ball1, ball2, ball3;
		private IContainer components;
		int point = 0;

		public FrmKeyDown()
		{
			player = new Ball(this, Brushes.Gray, ClientSize.Width / 2, ClientSize.Height - 60, 40, 40);
			ball1 = new Ball(this, Brushes.Red, 20, 20, 40, 40);
			ball2 = new Ball(this, Brushes.Green, 40, 20, 40, 40);
			ball3 = new Ball(this, Brushes.Green, 60, 20, 40, 40);
			InitializeComponent();
			RefreshLblPointCounter();
			timer.Start();
		}

		private void FrmKeyDown_Paint(object sender, PaintEventArgs e)
		{
			var rectangle = player.Rectangle;
			e.Graphics.FillEllipse(player.Brush, rectangle);
			if (rectangle.Right > ClientSize.Width)
			{
				rectangle.X -= ClientSize.Width;
				e.Graphics.FillEllipse(player.Brush, rectangle);
			}
			e.Graphics.DrawString("4", new Font(FontFamily.GenericSerif, 20), Brushes.Black, rectangle.Location);
		}

		class Ball
		{
			FrmKeyDown frmKeyDown;
			Rectangle rectangle;

			public Ball(FrmKeyDown frmKeyDown, Brush brush, Rectangle rectangle)
			{
				this.frmKeyDown = frmKeyDown;
				Brush = brush;
				this.rectangle = rectangle;
			}

			public Ball(FrmKeyDown frmKeyDown, Brush brush, int x, int y, int width, int height)
				: this(frmKeyDown, brush, new Rectangle(x, y, width, height)) { }

			public Brush Brush { get; }

			public Rectangle Rectangle => rectangle;

			public int X
			{
				get => rectangle.X;

				set
				{
					rectangle.X = value;
					if (rectangle.X < 0)
						rectangle.X = frmKeyDown.ClientSize.Width + rectangle.X;
					else if (rectangle.X > frmKeyDown.Width)
						rectangle.X %= frmKeyDown.ClientSize.Width;
				}
			}
			public int Y
			{
				get => rectangle.Y;

				set
				{
					rectangle.Y = value;
					if (rectangle.Top < 0)
						rectangle.Y = 0;
					else if (rectangle.Bottom > frmKeyDown.ClientSize.Height)
						rectangle.Y = frmKeyDown.ClientSize.Height - rectangle.Height;
				}
			}
		}

		private void FrmKeyDown_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
			case Keys.Left: player.X -= 15; break;
			case Keys.Right: player.X += 15; break;
			//case Keys.Up: black.Y -= 15; break;
			//case Keys.Down: black.Y += 15; break;

			default: return;
			}

			Invalidate();
		}

		private void Timer_Tick_Paint(object sender, PaintEventArgs e)
		{
			var rectangle = ball1.Rectangle;
			e.Graphics.FillEllipse(ball1.Brush, rectangle);
			if (rectangle.Right > ClientSize.Width)
			{
				rectangle.X -= ClientSize.Width;
				e.Graphics.FillEllipse(player.Brush, rectangle);
			}
			e.Graphics.DrawString("1", new Font(FontFamily.GenericSerif, 20), Brushes.Black, rectangle.Location);

			rectangle = ball2.Rectangle;
			e.Graphics.FillEllipse(ball2.Brush, rectangle);
			if (rectangle.Right > ClientSize.Width)
			{
				rectangle.X -= ClientSize.Width;
				e.Graphics.FillEllipse(player.Brush, rectangle);
			}
			e.Graphics.DrawString("2", new Font(FontFamily.GenericSerif, 20), Brushes.Black, rectangle.Location);

			rectangle = ball3.Rectangle;
			e.Graphics.FillEllipse(ball3.Brush, rectangle);
			if (rectangle.Right > ClientSize.Width)
			{
				rectangle.X -= ClientSize.Width;
				e.Graphics.FillEllipse(player.Brush, rectangle);
			}
			e.Graphics.DrawString("3", new Font(FontFamily.GenericSerif, 20), Brushes.Black, rectangle.Location);
		}

		private void FrmKeyDown_SizeChanged(object sender, EventArgs e)
		{
			Invalidate();
		}

		private void RefreshLblPointCounter()
		{
			lblPointCounter.Text = point.ToString();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && ( components != null ))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			ball1.X += random.Next(-20, 21);
			ball1.Y += random.Next(21);
			ball2.X += random.Next(-20, 21);
			ball2.Y += random.Next(21);
			ball3.X += random.Next(-20, 21);
			ball3.Y += random.Next(21);

			Invalidate();
		}
	}
}
