﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091103h
{
	public class Minbeon
	{
		static void Main(string[] args)
		{
			Console.WriteLine("주민번호 입력 하시오 예> 990101-1010101");
			Minbeon juminA0 = new Minbeon(Console.ReadLine());

			Console.WriteLine("당신이 태어난 년도는 " + juminA0.BirthDate.Year);
			Console.WriteLine("당신이 태어난 월은 " + juminA0.BirthDate.Month);
			Console.WriteLine("당신이 태어난 일은 " + juminA0.BirthDate.Day);
			Console.WriteLine("당신의 성별은 " + (0 == juminA0.GenderCode % 2 ? "여자" : "남자"));
			Console.WriteLine("당신의 나이는 " + new DateTime(DateTime.Now.CompareTo(juminA0.BirthDate)).Year);
		}

		private int halfBack;

		public DateTime BirthDate { get; }
		public TimeSpan Age => DateTime.Now - BirthDate;
		public int GenderCode { get; }

		public Minbeon(string id)
		{
			string trimmedId = id.Trim('-');
			if (13 != trimmedId.Length)
			{
				throw new FormatException("13 != trimmedId.Length");
			}

			foreach (char ch2 in trimmedId) if (!char.IsDigit(ch2)) throw new FormatException("not a number");
			
			this.halfBack = int.Parse(trimmedId.Substring(7));

			int year = int.Parse(trimmedId.Substring(0, 2));
			this.GenderCode = int.Parse(trimmedId.Substring(6, 1));
			switch (this.GenderCode)
			{
				case 1:
				case 2:
					year += 1900;
					break;
				case 3:
				case 4:
					year += 2000;
					break;
				default:
					throw new FormatException("GenderCode=" + this.GenderCode);
			}
			int month = int.Parse(trimmedId.Substring(2, 2));
			int day = int.Parse(trimmedId.Substring(4, 2));
			BirthDate = new DateTime(year, month, day);
		}
	}
}
