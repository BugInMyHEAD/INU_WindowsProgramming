﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP092001c
{
	class Program
	{
		static void Main(string[] args)
		{
			int[,] arr = { { 10, 20, 30 }, { 40, 50, 60 } };
			int i0 = 0;
			for (int i2 = 0; i2 < arr.GetLength(0); i2++)
			{
				for (int i4 = 0; i4 < arr.GetLength(1); i4++)
				{
					i0 += arr[i2, i4];
				}
			}
			Console.WriteLine(i0);
		}
	}
}
