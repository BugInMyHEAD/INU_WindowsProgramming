﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP090402h
{
	class GuessingGame
	{
		private static Random random = new Random();

		static void Main(string[] args)
		{
			new GuessingGame(1, 100).Play();
		}

		private int min, max;

		public GuessingGame(int min, int max)
		{
			if (min > max)
			{
				throw new ArgumentException("min > max");
			}

			if (min < 0)
			{
				throw new ArgumentException("min < 0");
			}

			this.min = min;
			this.max = max;
		}

		public void Play()
		{
			int a_su = random.Next() % ( max - min + 1 ) + min;  // 상대방이 생각한 임의의 수:각자 다르게 지정할것

			{
				int n1 = 1;
				/* 'do..while' 사용 시
				 * //do
				 */
				/* 'while' 사용 시
				 * //while(true)
				 */
				/* 'for' 사용 시 */
				for (; ; ) 
				{
					Console.Write(min + "에서 " + max + " 사이 상대가 생각하고 있는 수를 입력하세요:  ");
					int su = Convert.ToInt16(Console.ReadLine());

					if (su > a_su)
					{
						Console.WriteLine("너무 커");
					}
					else if (su < a_su)
					{
						Console.WriteLine("너무 작아");
					}
					else // when su == a_su
					{
						Console.WriteLine("정답");
						Console.WriteLine("시도 횟수 =" + n1);

						break;
					}
					
					n1++;
				}
				/* 'do..while' 사용 시
				 * // while(true);
				 */
			}
		}
	}
}
