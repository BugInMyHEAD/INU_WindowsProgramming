﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091102c
{
	class Score
	{
		static void Main(string[] args)
		{
			Student student = new Student();

			Console.Write("학번 입력 ");
			student.Id = int.Parse(Console.ReadLine());
			Console.Write("이름 입력 ");
			student.Name = Console.ReadLine();
			Console.Write("국어 입력 ");
			student.Kor = int.Parse(Console.ReadLine());
			Console.Write("영어 입력 ");
			student.Eng = int.Parse(Console.ReadLine());
			Console.Write("수학 입력 ");
			student.Mat = int.Parse(Console.ReadLine());

			Console.WriteLine(student);
		}
	}
}
