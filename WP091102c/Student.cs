﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091102c
{
	class Student
	{
		private int id;
		private string name;
		private int kor, eng, mat;

		public Student(int id, string name, int kor, int eng, int mat)
		{
			this.Id = id;
			this.Name = name;
			this.Kor = kor;
			this.Eng = eng;
			this.Mat = mat;
		}

		public Student()
		{
		}

		public int Id { get => id; set => id = value; }
		public string Name { get => name; set => name = value; }
		public int Kor { get => kor; set => kor = value; }
		public int Eng { get => eng; set => eng = value; }
		public int Mat { get => mat; set => mat = value; }

		public override string ToString()
		{
			return Id + " " + Name + " " + Kor + " " + Eng + " " + Mat + " " + Total() + " " + string.Format("{0:F2}", Average());
		}

		public int Total()
		{
			return Kor + Eng + Mat;
		}

		public double Average()
		{
			return (double)Total() / 3;
		}
	}
}
