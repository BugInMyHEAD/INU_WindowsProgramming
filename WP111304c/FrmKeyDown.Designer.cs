﻿namespace WP112001d
{
	partial class FrmKeyDown
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPointLable = new System.Windows.Forms.Label();
			this.lblPointCounter = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblPointLable
			// 
			this.lblPointLable.AutoSize = true;
			this.lblPointLable.Location = new System.Drawing.Point(13, 326);
			this.lblPointLable.Name = "lblPointLable";
			this.lblPointLable.Size = new System.Drawing.Size(37, 15);
			this.lblPointLable.TabIndex = 0;
			this.lblPointLable.Text = "점수";
			// 
			// lblPointCounter
			// 
			this.lblPointCounter.AutoSize = true;
			this.lblPointCounter.Location = new System.Drawing.Point(56, 326);
			this.lblPointCounter.Name = "lblPointCounter";
			this.lblPointCounter.Size = new System.Drawing.Size(12, 15);
			this.lblPointCounter.TabIndex = 0;
			this.lblPointCounter.Text = " ";
			// 
			// FrmKeyDown
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(382, 353);
			this.Controls.Add(this.lblPointCounter);
			this.Controls.Add(this.lblPointLable);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "FrmKeyDown";
			this.Text = "FrmKeyDown";
			this.SizeChanged += new System.EventHandler(this.FrmKeyDown_SizeChanged);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmKeyDown_Paint);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmKeyDown_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblPointLable;
		private System.Windows.Forms.Label lblPointCounter;
	}
}

