﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP112001d
{
	public partial class FrmKeyDown : Form
	{
		Ball black;
		Ball red;
		int point = 0;

		public FrmKeyDown()
		{
			black = new Ball(this, Brushes.Black, 60, 60, 40, 40);
			red = new Ball(this, Brushes.Red, 60, 60, 40, 40);
			InitializeComponent();
			RefreshLblPointCounter();
		}

		private void FrmKeyDown_Paint(object sender, PaintEventArgs e)
		{
			var rectangle = black.Rectangle;
			e.Graphics.FillEllipse(black.Brush, rectangle);
			if (rectangle.Right > ClientSize.Width)
			{
				rectangle.X -= ClientSize.Width;
				e.Graphics.FillEllipse(black.Brush, rectangle);
			}
		}

		class Ball
		{
			FrmKeyDown frmKeyDown;
			Rectangle rectangle;

			public Ball(FrmKeyDown frmKeyDown, Brush brush, Rectangle rectangle)
			{
				this.frmKeyDown = frmKeyDown;
				Brush = brush;
				this.rectangle = rectangle;
			}

			public Ball(FrmKeyDown frmKeyDown, Brush brush, int x, int y, int width, int height)
				: this(frmKeyDown, brush, new Rectangle(x, y, width, height)) { }

			public Brush Brush { get; }

			public Rectangle Rectangle => rectangle;

			public int X
			{
				get => rectangle.X;

				set
				{
					rectangle.X = value;
					if (rectangle.X < 0)
						rectangle.X = frmKeyDown.ClientSize.Width + rectangle.X;
					else if (rectangle.X > frmKeyDown.Width)
						rectangle.X %= frmKeyDown.ClientSize.Width;
				}
			}
			public int Y
			{
				get => rectangle.Y;

				set
				{
					rectangle.Y = value;
					if (rectangle.Top < 0)
						rectangle.Y = 0;
					else if (rectangle.Bottom > frmKeyDown.ClientSize.Height)
						rectangle.Y = frmKeyDown.ClientSize.Height - rectangle.Height;
				}
			}
		}

		private void FrmKeyDown_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
			case Keys.Left: black.X -= 20; break;
			case Keys.Right: black.X += 20; break;
			case Keys.Up: black.Y -= 20; break;
			case Keys.Down: black.Y += 20; break;

			default: return;
			}

			Invalidate();
		}

		private void FrmKeyDown_SizeChanged(object sender, EventArgs e)
		{
			Invalidate();
		}

		private void RefreshLblPointCounter()
		{
			lblPointCounter.Text = point.ToString();
		}
	}
}
