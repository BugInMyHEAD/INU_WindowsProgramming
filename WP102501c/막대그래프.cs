﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP102501c
{
	class 막대그래프
	{
		static void Main(string[] args)
		{
			string[] name = { "가서방", "나서방", "다서방", "라서방", "마서방", "바서방", };
			int[] score = { 67, 73, 99, 53, 88, 45 };

			//sorting
			IdxScore[] idxScore = new IdxScore[name.GetLength(0)];
			for (int i2 = 0; i2 < name.GetLength(0); i2++)
			{
				idxScore[i2] = new IdxScore(i2, score);
			}
			Array.Sort(idxScore);

			//printing
			foreach (var v2 in idxScore)
			{
				Console.Write($"{name[v2.idx]}    {v2.getScore()} ");
				v2.printGraph();
				Console.WriteLine(" " + v2.getGrade());
			}
		}
	}

	class IdxScore : IComparable<IdxScore>
	{
		public int idx;
		public int[] scoreArr;

		public IdxScore(int idx, int[] score)
		{
			this.idx = idx;
			this.scoreArr = score;
		}

		public int CompareTo(IdxScore obj)
		{
			return obj.getScore() - getScore();
		}

		public void printGraph()
		{
			for (var i2 = 0; i2 < scoreArr[idx]; i2 += 10)
			{
				Console.Write("*");
			}
		}

		public char getGrade()
		{
			var dividedScore = scoreArr[idx] / 10;
			switch (dividedScore)
			{
			case 10:
			case 9:
				return 'A';
			case 8:
				return 'B';
			case 7:
				return 'C';
			case 6:
				return 'D';
			default:
				return 'F';
			}
		}

		public int getScore()
		{
			return scoreArr[idx];
		}
	}
}
