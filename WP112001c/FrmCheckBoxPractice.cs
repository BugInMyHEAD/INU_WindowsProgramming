﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP112001c
{
	public partial class FrmCheckBoxPractice : Form
	{
		public FrmCheckBoxPractice()
		{
			InitializeComponent();
		}

		private void grpBox_Enter(object sender, EventArgs e)
		{

		}

		private void ChkStr_CheckedChanged(object sender, EventArgs e)
		{
			lblSample.Text = "";

			if (chkStr1.Checked)
			{
				lblSample.Text += chkStr1.Text + ", ";
			}

			if (chkStr2.Checked)
			{
				lblSample.Text += chkStr2.Text + ", ";
			}

			if (chkStr3.Checked)
			{
				lblSample.Text += chkStr3.Text;
			}
		}
	}
}
