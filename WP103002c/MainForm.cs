﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP103002c
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void BtnOutput_Click(object sender, EventArgs e)
		{
			txtOutput.Text = txtInput.Text;
		}

		private void BtnReset_Click(object sender, EventArgs e)
		{
			txtInput.Text = txtOutput.Text = "";
		}

		private void TxtInput_Click(object sender, EventArgs e)
		{
			txtInput.Text = "";
		}
	}
}
