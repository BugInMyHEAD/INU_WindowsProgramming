﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091301c
{
	class Login
	{
		static int Check(LinkedList<Login>.Enumerator argIt, string id, string pw)
		{
			while(argIt.MoveNext())
			{
				if(argIt.Current.id.Equals(id))
				{
					if(argIt.Current.pw.Equals(pw))
					{
						return 1;
					}
					else
					{
						return -1;
					}
				}
			}

			return 0;
		}

		static void PrintCommonMenu()
		{
			Console.WriteLine("게시판 읽기\n접속통계보기");
		}

		static void PrintMemberMenu()
		{
			Console.WriteLine("게시판 쓰기\n회원 패스워드 확인\n회원정보 수정");
		}

		static void Main(string[] args)
		{
			LinkedList<Login> members = new LinkedList<Login>();
			members.AddLast(new Login("kbs", "1234"));

			Console.Write("아이디를 입력하세요 : ");
			string loginTryingIdFromUser = Console.ReadLine();
			Console.Write("패스워드를 입력하세요 : ");
			string loginTryingPwFromUser = Console.ReadLine();
			int checkValue = Check(members.GetEnumerator(), loginTryingIdFromUser, loginTryingPwFromUser);
			if(0 == checkValue)
			{
				Console.WriteLine("회원가입해주세요\n");
				PrintCommonMenu();
			}
			else if(-1 == checkValue)
			{
				Console.WriteLine("패스워드를 정확히 입력하세요");
			}
			else if(1 == checkValue)
			{
				Console.WriteLine(loginTryingIdFromUser + "님 환영합니다");
				PrintCommonMenu();
				PrintMemberMenu();
			}
		}

		private string id;
		private string pw;

		public Login(string id, string pw)
		{
			this.id = id;
			this.pw = pw;
		}
	}
}
