﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP092002c
{
	class ScoreManager
	{
		static void Main(string[] args)
		{
			const int idIdx = 0;
			const int totIdx = 4;
			const int avgIdx = 5;
			const int rankIdx = 6;
			const int noOfSbj = totIdx - idIdx - 1;

			int[,] jumsu = new int[,]
			{
				{1, 66, 77, 88, 0, 0, 0},
				{2, 77, 88, 99, 0, 0, 0},
				{3, 44, 66, 88, 0, 0, 0},
				{4, 77, 55, 66, 0, 0, 0}
			};

			string[] name = new string[]
			{
				"KBS", "SBS", "YTN", "MBC"
			};

			for (var i2 = 0; i2 < jumsu.GetLength(0); i2++)
			{
				int i3;
				i3 = 0;
				for(int i4 = idIdx + 1; i4 < totIdx; i4++)
				{
					i3 += jumsu[i2, i4];
				}
				jumsu[i2, totIdx] = i3;
				jumsu[i2, avgIdx] = i3 / noOfSbj;
			}

			IdxTot[] forRanking = new IdxTot[jumsu.GetLength(0)];
			for (int i2 = 0; i2 < jumsu.GetLength(0); i2++)
			{
				forRanking[i2] = new IdxTot(i2, jumsu[i2, totIdx]);
			}
			Array.Sort(forRanking);
			for (int i2 = 0, j2 = 0; i2 < forRanking.Length; i2++)
			{
				if (i2 >= 1 && forRanking[i2].tot != forRanking[i2 - 1].tot)
				{
					j2 = i2;
				}
				jumsu[forRanking[i2].idx, rankIdx] = j2 + 1;
			}

			Console.WriteLine("학번 이름 국어 영어 수학 총점 평균 석차");
			for (var i2 = 0; i2 < jumsu.GetLength(0); i2++)
			{
				Console.Write($"{jumsu[i2, 0],4}");
				Console.Write($"{name[i2],4}");
				for (var i4 = 1; i4 < jumsu.GetLength(1); i4++)
				{
				Console.Write($"{jumsu[i2, i4],5}");
				}
				Console.WriteLine();
			}

			Console.WriteLine("============================정렬 후 데이터==============================");

			int[,] sortedJumsu = new int[jumsu.GetLength(0), jumsu.GetLength(1)];
			for (int i2 = 0; i2 < sortedJumsu.GetLength(0); i2++)
			{
				Array.Copy(jumsu, forRanking[i2].idx * jumsu.GetLength(1), sortedJumsu, i2 * sortedJumsu.GetLength(1), sortedJumsu.GetLength(1));
			}
			
			Console.WriteLine("학번 이름 국어 영어 수학 총점 평균 석차");
			for (var i2 = 0; i2 < sortedJumsu.GetLength(0); i2++)
			{
				Console.Write($"{sortedJumsu[i2, 0],4}");
				Console.Write($"{name[i2],4}");
				for (var i4 = 1; i4 < sortedJumsu.GetLength(1); i4++)
				{
					Console.Write($"{sortedJumsu[i2, i4],5}");
				}
				Console.WriteLine();
			}
		}
	}

	class IdxTot : IComparable<IdxTot>
	{
		public int idx, tot;

		public IdxTot(int idx, int tot)
		{
			this.idx = idx;
			this.tot = tot;
		}

		public int CompareTo(IdxTot obj)
		{
			return obj.tot - tot;
		}
	}
}
