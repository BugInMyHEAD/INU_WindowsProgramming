﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091802c
{
	class EvenOdd
	{
		static void Main(string[] args)
		{
			var eo = new EvenOdd();
			for (; ; )
			{
				Console.Write("정수 입력 : ");
				var intFromUser = int.Parse(Console.ReadLine());

				if (-1 == intFromUser)
					break;

				eo.Input(intFromUser);
			}
			Console.WriteLine("+++++++++++++++++++++++++++");
			Console.WriteLine("데이터 개수 " + eo.Count);
			Console.WriteLine("짝수의 합 " + eo.EvenSum);
			Console.WriteLine("짝수의 개수 " + eo.EvenCount);
			Console.WriteLine("홀수의 합 " + eo.OddSum);
			Console.WriteLine("홀수의 개수 " + eo.OddCount);
		}

		public int Count => EvenCount + OddCount;
		public int Sum => EvenSum + OddSum;
		public int EvenCount { get; protected set; }
		public int OddCount { get; protected set; }
		public int EvenSum { get; protected set; }
		public int OddSum { get; protected set; }

		public void Input(int i)
		{
			if (0 == i % 2)
			{
				++EvenCount;
				EvenSum += i;
			}
			else
			{
				++OddCount;
				OddSum += i;
			}
		}
	}
}
