﻿namespace WP112002c
{
	partial class FrmCheckedListBox
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.chklisSubject = new System.Windows.Forms.CheckedListBox();
			this.lblSubject = new System.Windows.Forms.Label();
			this.btnSelect = new System.Windows.Forms.Button();
			this.btnExit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// chklisSubject
			// 
			this.chklisSubject.FormattingEnabled = true;
			this.chklisSubject.Items.AddRange(new object[] {
            "C언어",
            "Visual C++",
            "C#",
            "Java",
            "JSP",
            "ASP",
            "PHP"});
			this.chklisSubject.Location = new System.Drawing.Point(63, 48);
			this.chklisSubject.Name = "chklisSubject";
			this.chklisSubject.Size = new System.Drawing.Size(120, 84);
			this.chklisSubject.TabIndex = 0;
			// 
			// lblSubject
			// 
			this.lblSubject.AutoSize = true;
			this.lblSubject.Location = new System.Drawing.Point(63, 159);
			this.lblSubject.Name = "lblSubject";
			this.lblSubject.Size = new System.Drawing.Size(157, 12);
			this.lblSubject.TabIndex = 1;
			this.lblSubject.Text = "좋아하는 과목을 선택하세요";
			// 
			// btnSelect
			// 
			this.btnSelect.Location = new System.Drawing.Point(43, 206);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(75, 23);
			this.btnSelect.TabIndex = 2;
			this.btnSelect.Text = "선택";
			this.btnSelect.UseVisualStyleBackColor = true;
			this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
			// 
			// btnExit
			// 
			this.btnExit.Location = new System.Drawing.Point(145, 206);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(75, 23);
			this.btnExit.TabIndex = 2;
			this.btnExit.Text = "종료";
			this.btnExit.UseVisualStyleBackColor = true;
			// 
			// FrmCheckedListBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.btnSelect);
			this.Controls.Add(this.lblSubject);
			this.Controls.Add(this.chklisSubject);
			this.Name = "FrmCheckedListBox";
			this.RightToLeftLayout = true;
			this.Text = "체크리스트박스 연습";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckedListBox chklisSubject;
		private System.Windows.Forms.Label lblSubject;
		private System.Windows.Forms.Button btnSelect;
		private System.Windows.Forms.Button btnExit;
	}
}

