﻿namespace WP120401c
{
	partial class MainForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainMenu = new System.Windows.Forms.MenuStrip();
			this.menu_File = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_New = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_Open = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_Save = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.mi_PageSetup = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_Print = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.mi_Exit = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Edit = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Font = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_View = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_Help = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_MyApp = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_SRPGameGraphic = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_IURandomTest = new System.Windows.Forms.ToolStripMenuItem();
			this.mi_Calculator = new System.Windows.Forms.ToolStripMenuItem();
			this.txtMemo = new System.Windows.Forms.TextBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.printDialog1 = new System.Windows.Forms.PrintDialog();
			this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
			this.MainMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// MainMenu
			// 
			this.MainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_File,
            this.menu_Edit,
            this.menu_Font,
            this.menu_View,
            this.menu_Help,
            this.menu_MyApp});
			this.MainMenu.Location = new System.Drawing.Point(0, 0);
			this.MainMenu.Name = "MainMenu";
			this.MainMenu.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
			this.MainMenu.Size = new System.Drawing.Size(470, 28);
			this.MainMenu.TabIndex = 0;
			this.MainMenu.Text = "menuStrip1";
			// 
			// menu_File
			// 
			this.menu_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi_New,
            this.mi_Open,
            this.mi_Save,
            this.mi_SaveAs,
            this.toolStripMenuItem1,
            this.mi_PageSetup,
            this.mi_Print,
            this.toolStripMenuItem2,
            this.mi_Exit});
			this.menu_File.Name = "menu_File";
			this.menu_File.Size = new System.Drawing.Size(68, 24);
			this.menu_File.Text = "파일(&F)";
			// 
			// mi_New
			// 
			this.mi_New.Name = "mi_New";
			this.mi_New.ShortcutKeyDisplayString = "";
			this.mi_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.mi_New.Size = new System.Drawing.Size(297, 26);
			this.mi_New.Text = "새로 만들기(&N)";
			this.mi_New.Click += new System.EventHandler(this.Mi_New_Click);
			// 
			// mi_Open
			// 
			this.mi_Open.Name = "mi_Open";
			this.mi_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.mi_Open.Size = new System.Drawing.Size(297, 26);
			this.mi_Open.Text = "열기(&O)...";
			this.mi_Open.Click += new System.EventHandler(this.Mi_Open_Click);
			// 
			// mi_Save
			// 
			this.mi_Save.Name = "mi_Save";
			this.mi_Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.mi_Save.Size = new System.Drawing.Size(297, 26);
			this.mi_Save.Text = "저장(&S)";
			this.mi_Save.Click += new System.EventHandler(this.Mi_Save_Click);
			// 
			// mi_SaveAs
			// 
			this.mi_SaveAs.Name = "mi_SaveAs";
			this.mi_SaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
			this.mi_SaveAs.Size = new System.Drawing.Size(297, 26);
			this.mi_SaveAs.Text = "다른 이름으로 저장(&A)...";
			this.mi_SaveAs.Click += new System.EventHandler(this.Mi_SaveAs_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(294, 6);
			// 
			// mi_PageSetup
			// 
			this.mi_PageSetup.Name = "mi_PageSetup";
			this.mi_PageSetup.Size = new System.Drawing.Size(297, 26);
			this.mi_PageSetup.Text = "페이지 설정(&U)...";
			this.mi_PageSetup.Click += new System.EventHandler(this.Mi_PageSetup_Click);
			// 
			// mi_Print
			// 
			this.mi_Print.Name = "mi_Print";
			this.mi_Print.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
			this.mi_Print.Size = new System.Drawing.Size(297, 26);
			this.mi_Print.Text = "인쇄(&P)...";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(294, 6);
			// 
			// mi_Exit
			// 
			this.mi_Exit.Name = "mi_Exit";
			this.mi_Exit.Size = new System.Drawing.Size(297, 26);
			this.mi_Exit.Text = "끝내기(&X)";
			// 
			// menu_Edit
			// 
			this.menu_Edit.Name = "menu_Edit";
			this.menu_Edit.Size = new System.Drawing.Size(69, 24);
			this.menu_Edit.Text = "편집(&E)";
			// 
			// menu_Font
			// 
			this.menu_Font.Name = "menu_Font";
			this.menu_Font.Size = new System.Drawing.Size(73, 24);
			this.menu_Font.Text = "서식(&O)";
			// 
			// menu_View
			// 
			this.menu_View.Name = "menu_View";
			this.menu_View.Size = new System.Drawing.Size(71, 24);
			this.menu_View.Text = "보기(&V)";
			// 
			// menu_Help
			// 
			this.menu_Help.Name = "menu_Help";
			this.menu_Help.Size = new System.Drawing.Size(87, 24);
			this.menu_Help.Text = "도움말(&H)";
			// 
			// menu_MyApp
			// 
			this.menu_MyApp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi_SRPGameGraphic,
            this.mi_IURandomTest,
            this.mi_Calculator});
			this.menu_MyApp.Name = "menu_MyApp";
			this.menu_MyApp.Size = new System.Drawing.Size(71, 24);
			this.menu_MyApp.Text = "내 작품";
			// 
			// mi_SRPGameGraphic
			// 
			this.mi_SRPGameGraphic.Name = "mi_SRPGameGraphic";
			this.mi_SRPGameGraphic.Size = new System.Drawing.Size(189, 26);
			this.mi_SRPGameGraphic.Text = "가위바위보게임";
			this.mi_SRPGameGraphic.Click += new System.EventHandler(this.Mi_SRPGameGraphic_Click);
			// 
			// mi_IURandomTest
			// 
			this.mi_IURandomTest.Name = "mi_IURandomTest";
			this.mi_IURandomTest.Size = new System.Drawing.Size(189, 26);
			this.mi_IURandomTest.Text = "랜덤시험문제";
			this.mi_IURandomTest.Click += new System.EventHandler(this.Mi_IURandomTest_Click);
			// 
			// mi_Calculator
			// 
			this.mi_Calculator.Name = "mi_Calculator";
			this.mi_Calculator.Size = new System.Drawing.Size(189, 26);
			this.mi_Calculator.Text = "마이계산기";
			this.mi_Calculator.Click += new System.EventHandler(this.Mi_Calculator_Click);
			// 
			// txtMemo
			// 
			this.txtMemo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtMemo.Location = new System.Drawing.Point(0, 28);
			this.txtMemo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtMemo.Multiline = true;
			this.txtMemo.Name = "txtMemo";
			this.txtMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtMemo.Size = new System.Drawing.Size(470, 298);
			this.txtMemo.TabIndex = 1;
			this.txtMemo.TextChanged += new System.EventHandler(this.TxtMemo_TextChanged);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.Filter = "텍스트문서(*.txt)|*.txt|모든파일(*.*)|*.*";
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.Filter = "텍스트문서(*.txt)|*.txt|모든파일(*.*)|*.*";
			// 
			// printDialog1
			// 
			this.printDialog1.UseEXDialog = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(470, 326);
			this.Controls.Add(this.txtMemo);
			this.Controls.Add(this.MainMenu);
			this.MainMenuStrip = this.MainMenu;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "MainForm";
			this.Text = "제목없음";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.MainMenu.ResumeLayout(false);
			this.MainMenu.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip MainMenu;
		private System.Windows.Forms.ToolStripMenuItem menu_File;
		private System.Windows.Forms.ToolStripMenuItem mi_New;
		private System.Windows.Forms.ToolStripMenuItem mi_Open;
		private System.Windows.Forms.ToolStripMenuItem mi_Save;
		private System.Windows.Forms.ToolStripMenuItem mi_SaveAs;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem mi_PageSetup;
		private System.Windows.Forms.ToolStripMenuItem mi_Print;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem mi_Exit;
		private System.Windows.Forms.ToolStripMenuItem menu_Edit;
		private System.Windows.Forms.ToolStripMenuItem menu_Font;
		private System.Windows.Forms.ToolStripMenuItem menu_View;
		private System.Windows.Forms.ToolStripMenuItem menu_Help;
		private System.Windows.Forms.ToolStripMenuItem menu_MyApp;
		public System.Windows.Forms.TextBox txtMemo;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.PrintDialog printDialog1;
		private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
		private System.Windows.Forms.ToolStripMenuItem mi_SRPGameGraphic;
		private System.Windows.Forms.ToolStripMenuItem mi_IURandomTest;
		private System.Windows.Forms.ToolStripMenuItem mi_Calculator;
	}
}

