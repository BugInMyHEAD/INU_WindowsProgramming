﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WP120401c
{
	public partial class MainForm : Form
	{
		bool isTextChanged;

		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			//openFileDialog1.ShowDialog();
		}

		private void Mi_New_Click(object sender, EventArgs e)
		{
			SaveOrClearOrCancel("New");
		}

		private void SaveOrClearOrCancel(string strFlag)
		{
			if (isTextChanged) // 텍스트 박스 내용이 변경되었으면 메세지박스 호출
			{
				DialogResult objDr = MessageBox.Show(
					 "파일내용이 변경되었습니다.\r\n" +
					 "변경된 내용을 저장하시겠습니까 ?", "메모장",
					 MessageBoxButtons.YesNoCancel,  // 버튼 3개 표시
					 MessageBoxIcon.Exclamation);       // 아이콘 표시
				switch (objDr)
				{
				case DialogResult.Yes:    // 저장할거면 – Yes 버튼이면
					SaveText();   // 텍스트 저장
					if (strFlag == "New")  // Yes이고 매개변수가 New 일 때 
						ClearText(); // 텍스트 클리어(리셋)
					else
						OpenText(); // 열기
					break;
				case DialogResult.No:   // 저장 안할거면 - NO 버튼이면
					if (strFlag == "New") // No이고 매개변수가 New 일  때 
						ClearText(); // 텍스트 클리어(리셋)
					else
						OpenText(); // 열기
					break;
				case DialogResult.Cancel:   // 취소면 그대로 유지
					break;

				}
			}
			else
			{
				ClearText();
			}
		}

		private void ClearText()
		{
			// 새로만들기 - 텍스트 박스 내용이 변경되지 않았으면
			this.txtMemo.ResetText(); // 텍스트 박스 리셋 호출
			this.Text = "제목없음"; // 제목 다시 설정 – 아무 입력 없으므로 
			isTextChanged = false; // 다시 설정
		}

		private void OpenText()
		{
			DialogResult objDr = openFileDialog1.ShowDialog(); //열기 대화상자
			if (objDr != DialogResult.Cancel)  // ShowDialog() 취소버튼이 아니라면
			{
				string strFileName = openFileDialog1.FileName;
				// 열기 대화상자에서 입력한 파일명 으로 객체 생성
				StreamReader objSr = new StreamReader(strFileName);
				// 해당 파일에서 처음부터 끝까지 Read
				this.txtMemo.Text = objSr.ReadToEnd();   // 읽어서 텍스트 박스에 붙임
				objSr.Close();
				isTextChanged = false;   // 변경여부를 다시 초기화
				this.Text = strFileName;  // 제목에 파일명 출력
			}
		}

		private void SaveText()
		{
			if (this.Text == "제목없음")  // 한번도 저장되지 않은 문서라면
			{
				DialogResult objDr = saveFileDialog1.ShowDialog();
				if (objDr != DialogResult.Cancel)// 예 버튼 클릭하면 -저장할거면
				{
					string strFileName = saveFileDialog1.FileName;  // 대화상자에서 입력한 파일명
					SaveFile(strFileName); // 저장기능 메소드호출-매개변수는 파일명
				}
			}
			else  // 한번이라도 저장된 문서라면
			{
				string strFileName = this.Text; // 파일 전체 이름
				SaveFile(strFileName); // 저장기능 메소드호출-매개변수는 파일명
			}
		}

		private void SaveFile(string strFileName)  // 저장하는 메소드 기능
		{
			StreamWriter objSw = new StreamWriter(strFileName);
			objSw.Write(this.txtMemo.Text);
			objSw.Flush();  // 스트림의 내용 비움
			objSw.Close();

			isTextChanged = false;  // 저장하고 나서 다시 false로 설정
			Text = strFileName;
		}


		private void TxtMemo_TextChanged(object sender, EventArgs e)
		{
			isTextChanged = true;
		}

		private void Mi_SRPGameGraphic_Click(object sender, EventArgs e)
		{
			const string filename = "WP110601c.exe";
			if (File.Exists(filename))
			{
				System.Diagnostics.Process.Start(filename);
			}
			else
			{
				MessageBox.Show("가위바위보게임 실행 파일이 없습니다.", " 메모장",
				   MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

		}

		private void Mi_IURandomTest_Click(object sender, EventArgs e)
		{
			const string filename = "WP112701d.exe";
			if (File.Exists(filename))
			{
				System.Diagnostics.Process.Start(filename);
			}
			else
			{
				MessageBox.Show("IURandomTest 실행 파일이 없습니다.", " 메모장",
				   MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void Mi_Calculator_Click(object sender, EventArgs e)
		{
			const string filename = "WP112702c.exe";
			if (File.Exists(filename))
			{
				System.Diagnostics.Process.Start(filename);
			}
			else
			{
				MessageBox.Show("계산기 실행 파일이 없습니다.", " 메모장",
				   MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void Mi_Open_Click(object sender, EventArgs e)
		{

		}

		private void Mi_Save_Click(object sender, EventArgs e)
		{
			SaveText();
		}

		private void Mi_SaveAs_Click(object sender, EventArgs e)
		{
			// 텍스트 내용이 변경되던 아니든 무조건 저장하기 창 띄워 줌
			DialogResult objDr = saveFileDialog1.ShowDialog();
			if (objDr != DialogResult.Cancel)  // 취소 버튼이 아니면
			{
				string strFileName = saveFileDialog1.FileName;  // 대화상자에서 입력한 이름
				SaveFile(strFileName); // 파일이름 매개변수로 저장하기 호출
			}
		}

		private void Mi_PageSetup_Click(object sender, EventArgs e)
		{
			//[1] PrintDocument.DocumentName = 문서 지정
			printDocument1.DocumentName = txtMemo.Text;
			//[2] PageSetupDialog.Document = PrintDocument
			pageSetupDialog1.Document = this.printDocument1;
			//[3] 페이지 설정 창 띄우기
			pageSetupDialog1.ShowDialog();

		}
	}
}
