﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP101801c
{
	class Triangle
	{
		static void Main(string[] args)
		{
			Triangle triangle = new Triangle();
			triangle.Width = 10;
			triangle.Height = 20;
			Console.WriteLine($"삼각형 1의 밑변{triangle.Width}, 높이{triangle.Height}, 면적{triangle.Area}");
		}

		public int Width { get; set; }
		public int Height { get => Height; set => Height = value; }
		public int Area => Width * Height / 2;
	}
}
