﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WP091103h;

namespace WP111501c
{
	public partial class FrmMain : Form
	{
		public FrmMain()
		{
			InitializeComponent();
		}

		private void BtnMinbeonCheck_Click(object sender, EventArgs e)
		{
			var minbeon = new Minbeon(txtMinbeonFront.Text + txtMinbeonBack.Text);
			txtBirthYear.Text = minbeon.BirthDate.Year.ToString();
			txtBirthMonth.Text = minbeon.BirthDate.Month.ToString();
			txtBirthDay.Text = minbeon.BirthDate.Day.ToString();
			txtGender.Text = minbeon.GenderCode % 2 == 0 ? "여자" : "남자";
			txtAge.Text = ( (int)Math.Floor(minbeon.Age.TotalDays / DateTime.MaxValue.DayOfYear) ).ToString();
		}

		private void TxtMinbeonFront_TextChanged(object sender, EventArgs e)
		{
			if (6 == txtMinbeonFront.TextLength)
			{
				txtMinbeonBack.Focus();
			}
		}

		private void TxtMinbeonBack_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Enter)
			{
				BtnMinbeonCheck_Click(btnMinbeonCheck, e);
			}
		}
	}
}
