﻿namespace WP111501c
{
	partial class FrmMain
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblMinbeon = new System.Windows.Forms.Label();
			this.txtMinbeonFront = new System.Windows.Forms.TextBox();
			this.txtMinbeonBack = new System.Windows.Forms.TextBox();
			this.lblJuminHyphen = new System.Windows.Forms.Label();
			this.btnMinbeonCheck = new System.Windows.Forms.Button();
			this.lblBirthDate = new System.Windows.Forms.Label();
			this.txtBirthYear = new System.Windows.Forms.TextBox();
			this.txtBirthMonth = new System.Windows.Forms.TextBox();
			this.lblBirthYear = new System.Windows.Forms.Label();
			this.lblBirthMonth = new System.Windows.Forms.Label();
			this.lblBirthDayOfMonth = new System.Windows.Forms.Label();
			this.txtBirthDay = new System.Windows.Forms.TextBox();
			this.lblGender = new System.Windows.Forms.Label();
			this.lblAge = new System.Windows.Forms.Label();
			this.txtGender = new System.Windows.Forms.TextBox();
			this.txtAge = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblMinbeon
			// 
			this.lblMinbeon.AutoSize = true;
			this.lblMinbeon.Location = new System.Drawing.Point(14, 39);
			this.lblMinbeon.Name = "lblMinbeon";
			this.lblMinbeon.Size = new System.Drawing.Size(67, 15);
			this.lblMinbeon.TabIndex = 0;
			this.lblMinbeon.Text = "주민번호";
			// 
			// txtMinbeonFront
			// 
			this.txtMinbeonFront.Location = new System.Drawing.Point(81, 35);
			this.txtMinbeonFront.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtMinbeonFront.Name = "txtMinbeonFront";
			this.txtMinbeonFront.Size = new System.Drawing.Size(63, 25);
			this.txtMinbeonFront.TabIndex = 1;
			this.txtMinbeonFront.TextChanged += new System.EventHandler(this.TxtMinbeonFront_TextChanged);
			// 
			// txtMinbeonBack
			// 
			this.txtMinbeonBack.Location = new System.Drawing.Point(171, 35);
			this.txtMinbeonBack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtMinbeonBack.Name = "txtMinbeonBack";
			this.txtMinbeonBack.Size = new System.Drawing.Size(63, 25);
			this.txtMinbeonBack.TabIndex = 2;
			this.txtMinbeonBack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtMinbeonBack_KeyPress);
			// 
			// lblJuminHyphen
			// 
			this.lblJuminHyphen.AutoSize = true;
			this.lblJuminHyphen.Location = new System.Drawing.Point(152, 39);
			this.lblJuminHyphen.Name = "lblJuminHyphen";
			this.lblJuminHyphen.Size = new System.Drawing.Size(15, 15);
			this.lblJuminHyphen.TabIndex = 0;
			this.lblJuminHyphen.Text = "-";
			// 
			// btnMinbeonCheck
			// 
			this.btnMinbeonCheck.Location = new System.Drawing.Point(112, 69);
			this.btnMinbeonCheck.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnMinbeonCheck.Name = "btnMinbeonCheck";
			this.btnMinbeonCheck.Size = new System.Drawing.Size(86, 29);
			this.btnMinbeonCheck.TabIndex = 3;
			this.btnMinbeonCheck.Text = "확인";
			this.btnMinbeonCheck.UseVisualStyleBackColor = true;
			this.btnMinbeonCheck.Click += new System.EventHandler(this.BtnMinbeonCheck_Click);
			// 
			// lblBirthDate
			// 
			this.lblBirthDate.AutoSize = true;
			this.lblBirthDate.Location = new System.Drawing.Point(14, 142);
			this.lblBirthDate.Name = "lblBirthDate";
			this.lblBirthDate.Size = new System.Drawing.Size(67, 15);
			this.lblBirthDate.TabIndex = 0;
			this.lblBirthDate.Text = "생년월일";
			// 
			// txtBirthYear
			// 
			this.txtBirthYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtBirthYear.Location = new System.Drawing.Point(81, 139);
			this.txtBirthYear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtBirthYear.Name = "txtBirthYear";
			this.txtBirthYear.ReadOnly = true;
			this.txtBirthYear.Size = new System.Drawing.Size(43, 25);
			this.txtBirthYear.TabIndex = 0;
			this.txtBirthYear.TabStop = false;
			// 
			// txtBirthMonth
			// 
			this.txtBirthMonth.BackColor = System.Drawing.SystemColors.Window;
			this.txtBirthMonth.Location = new System.Drawing.Point(158, 139);
			this.txtBirthMonth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtBirthMonth.Name = "txtBirthMonth";
			this.txtBirthMonth.ReadOnly = true;
			this.txtBirthMonth.Size = new System.Drawing.Size(29, 25);
			this.txtBirthMonth.TabIndex = 0;
			this.txtBirthMonth.TabStop = false;
			// 
			// lblBirthYear
			// 
			this.lblBirthYear.AutoSize = true;
			this.lblBirthYear.Location = new System.Drawing.Point(131, 142);
			this.lblBirthYear.Name = "lblBirthYear";
			this.lblBirthYear.Size = new System.Drawing.Size(22, 15);
			this.lblBirthYear.TabIndex = 0;
			this.lblBirthYear.Text = "년";
			// 
			// lblBirthMonth
			// 
			this.lblBirthMonth.AutoSize = true;
			this.lblBirthMonth.Location = new System.Drawing.Point(194, 142);
			this.lblBirthMonth.Name = "lblBirthMonth";
			this.lblBirthMonth.Size = new System.Drawing.Size(22, 15);
			this.lblBirthMonth.TabIndex = 0;
			this.lblBirthMonth.Text = "월";
			// 
			// lblBirthDayOfMonth
			// 
			this.lblBirthDayOfMonth.AutoSize = true;
			this.lblBirthDayOfMonth.Location = new System.Drawing.Point(257, 142);
			this.lblBirthDayOfMonth.Name = "lblBirthDayOfMonth";
			this.lblBirthDayOfMonth.Size = new System.Drawing.Size(22, 15);
			this.lblBirthDayOfMonth.TabIndex = 0;
			this.lblBirthDayOfMonth.Text = "일";
			// 
			// txtBirthDay
			// 
			this.txtBirthDay.BackColor = System.Drawing.SystemColors.Window;
			this.txtBirthDay.Location = new System.Drawing.Point(221, 139);
			this.txtBirthDay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtBirthDay.Name = "txtBirthDay";
			this.txtBirthDay.ReadOnly = true;
			this.txtBirthDay.Size = new System.Drawing.Size(29, 25);
			this.txtBirthDay.TabIndex = 0;
			this.txtBirthDay.TabStop = false;
			// 
			// lblGender
			// 
			this.lblGender.AutoSize = true;
			this.lblGender.Location = new System.Drawing.Point(14, 198);
			this.lblGender.Name = "lblGender";
			this.lblGender.Size = new System.Drawing.Size(37, 15);
			this.lblGender.TabIndex = 0;
			this.lblGender.Text = "성별";
			// 
			// lblAge
			// 
			this.lblAge.AutoSize = true;
			this.lblAge.Location = new System.Drawing.Point(14, 254);
			this.lblAge.Name = "lblAge";
			this.lblAge.Size = new System.Drawing.Size(37, 15);
			this.lblAge.TabIndex = 0;
			this.lblAge.Text = "나이";
			// 
			// txtGender
			// 
			this.txtGender.BackColor = System.Drawing.SystemColors.Window;
			this.txtGender.Location = new System.Drawing.Point(81, 194);
			this.txtGender.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtGender.Name = "txtGender";
			this.txtGender.ReadOnly = true;
			this.txtGender.Size = new System.Drawing.Size(43, 25);
			this.txtGender.TabIndex = 0;
			this.txtGender.TabStop = false;
			// 
			// txtAge
			// 
			this.txtAge.BackColor = System.Drawing.SystemColors.Window;
			this.txtAge.Location = new System.Drawing.Point(81, 250);
			this.txtAge.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.txtAge.Name = "txtAge";
			this.txtAge.ReadOnly = true;
			this.txtAge.Size = new System.Drawing.Size(43, 25);
			this.txtAge.TabIndex = 0;
			this.txtAge.TabStop = false;
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(325, 326);
			this.Controls.Add(this.btnMinbeonCheck);
			this.Controls.Add(this.txtMinbeonBack);
			this.Controls.Add(this.txtBirthDay);
			this.Controls.Add(this.txtBirthMonth);
			this.Controls.Add(this.txtAge);
			this.Controls.Add(this.txtGender);
			this.Controls.Add(this.txtBirthYear);
			this.Controls.Add(this.txtMinbeonFront);
			this.Controls.Add(this.lblBirthDayOfMonth);
			this.Controls.Add(this.lblJuminHyphen);
			this.Controls.Add(this.lblBirthMonth);
			this.Controls.Add(this.lblBirthYear);
			this.Controls.Add(this.lblAge);
			this.Controls.Add(this.lblGender);
			this.Controls.Add(this.lblBirthDate);
			this.Controls.Add(this.lblMinbeon);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.MaximizeBox = false;
			this.Name = "FrmMain";
			this.Text = "과제3";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblMinbeon;
		private System.Windows.Forms.TextBox txtMinbeonFront;
		private System.Windows.Forms.TextBox txtMinbeonBack;
		private System.Windows.Forms.Label lblJuminHyphen;
		private System.Windows.Forms.Button btnMinbeonCheck;
		private System.Windows.Forms.Label lblBirthDate;
		private System.Windows.Forms.TextBox txtBirthYear;
		private System.Windows.Forms.TextBox txtBirthMonth;
		private System.Windows.Forms.Label lblBirthYear;
		private System.Windows.Forms.Label lblBirthMonth;
		private System.Windows.Forms.Label lblBirthDayOfMonth;
		private System.Windows.Forms.TextBox txtBirthDay;
		private System.Windows.Forms.Label lblGender;
		private System.Windows.Forms.Label lblAge;
		private System.Windows.Forms.TextBox txtGender;
		private System.Windows.Forms.TextBox txtAge;
	}
}

