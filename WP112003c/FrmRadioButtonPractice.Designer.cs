﻿namespace WP112003c
{
	partial class FrmRadioButtonPractice
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.radFemale = new System.Windows.Forms.RadioButton();
			this.radMale = new System.Windows.Forms.RadioButton();
			this.radAndrogyny = new System.Windows.Forms.RadioButton();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(35, 54);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(53, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "성별입력";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(110, 51);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(67, 21);
			this.textBox1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(70, 99);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "확인";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// radFemale
			// 
			this.radFemale.AutoSize = true;
			this.radFemale.Location = new System.Drawing.Point(23, 44);
			this.radFemale.Name = "radFemale";
			this.radFemale.Size = new System.Drawing.Size(47, 16);
			this.radFemale.TabIndex = 3;
			this.radFemale.TabStop = true;
			this.radFemale.Text = "여성";
			this.radFemale.UseVisualStyleBackColor = true;
			// 
			// radMale
			// 
			this.radMale.AutoSize = true;
			this.radMale.Location = new System.Drawing.Point(76, 44);
			this.radMale.Name = "radMale";
			this.radMale.Size = new System.Drawing.Size(47, 16);
			this.radMale.TabIndex = 3;
			this.radMale.TabStop = true;
			this.radMale.Text = "남성";
			this.radMale.UseVisualStyleBackColor = true;
			// 
			// radAndrogyny
			// 
			this.radAndrogyny.AutoSize = true;
			this.radAndrogyny.Location = new System.Drawing.Point(129, 44);
			this.radAndrogyny.Name = "radAndrogyny";
			this.radAndrogyny.Size = new System.Drawing.Size(47, 16);
			this.radAndrogyny.TabIndex = 3;
			this.radAndrogyny.TabStop = true;
			this.radAndrogyny.Text = "중성";
			this.radAndrogyny.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.radAndrogyny);
			this.panel1.Controls.Add(this.radMale);
			this.panel1.Controls.Add(this.radFemale);
			this.panel1.Location = new System.Drawing.Point(31, 155);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(222, 85);
			this.panel1.TabIndex = 4;
			// 
			// FrmRadioButtonPractice
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label1);
			this.Name = "FrmRadioButtonPractice";
			this.Text = "R_B_과제";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.RadioButton radFemale;
		private System.Windows.Forms.RadioButton radMale;
		private System.Windows.Forms.RadioButton radAndrogyny;
		private System.Windows.Forms.Panel panel1;
	}
}

