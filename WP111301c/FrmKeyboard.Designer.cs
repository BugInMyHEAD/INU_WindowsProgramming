﻿namespace WP111301c
{
	partial class FrmKeyboard
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblKorean = new System.Windows.Forms.Label();
			this.lblEnglish = new System.Windows.Forms.Label();
			this.txtKorean = new System.Windows.Forms.TextBox();
			this.txtEnglish = new System.Windows.Forms.TextBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblKorean
			// 
			this.lblKorean.AutoSize = true;
			this.lblKorean.Location = new System.Drawing.Point(26, 23);
			this.lblKorean.Name = "lblKorean";
			this.lblKorean.Size = new System.Drawing.Size(29, 12);
			this.lblKorean.TabIndex = 0;
			this.lblKorean.Text = Properties.Resources.kr_Korean;
			// 
			// lblEnglish
			// 
			this.lblEnglish.AutoSize = true;
			this.lblEnglish.Location = new System.Drawing.Point(26, 52);
			this.lblEnglish.Name = "lblEnglish";
			this.lblEnglish.Size = new System.Drawing.Size(29, 12);
			this.lblEnglish.TabIndex = 0;
			this.lblEnglish.Text = Properties.Resources.kr_English;
			// 
			// txtKorean
			// 
			this.txtKorean.Location = new System.Drawing.Point(97, 20);
			this.txtKorean.Name = "txtKorean";
			this.txtKorean.Size = new System.Drawing.Size(100, 21);
			this.txtKorean.TabIndex = 1;
			this.txtKorean.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtKorean_KeyPress);
			// 
			// txtEnglish
			// 
			this.txtEnglish.Location = new System.Drawing.Point(97, 49);
			this.txtEnglish.Name = "txtEnglish";
			this.txtEnglish.Size = new System.Drawing.Size(100, 21);
			this.txtEnglish.TabIndex = 1;
			// 
			// btnOk
			// 
			this.btnOk.Location = new System.Drawing.Point(70, 92);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 2;
			this.btnOk.Text = "OK";
			this.btnOk.UseVisualStyleBackColor = true;
			// 
			// FrmKeyboard
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.txtEnglish);
			this.Controls.Add(this.txtKorean);
			this.Controls.Add(this.lblEnglish);
			this.Controls.Add(this.lblKorean);
			this.Name = "FrmKeyboard";
			this.Text = "FrmKeyboard";
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		#endregion

		private System.Windows.Forms.Label lblKorean;
		private System.Windows.Forms.Label lblEnglish;
		private System.Windows.Forms.TextBox txtKorean;
		private System.Windows.Forms.TextBox txtEnglish;
		private System.Windows.Forms.Button btnOk;
	}
}

