﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WP091302h;

namespace WP110601c
{
	public partial class SRPGameForm : Form
	{
		SRPGame.Player computer = new SRPGame.Player();
		SRPGame.Player player = new SRPGame.Player();
		SRPGame srpGame;
		bool isStatViewShowing = true;
		int widthChangePerTickEvent = 10;

		public SRPGameForm()
		{
			srpGame = new SRPGame(computer, player);
			InitializeComponent();
			UpdateStat();
			BtnStat_Click(this, null);
		}

		private void BtnSRP_Click(object sender, EventArgs e)
		{
			Button btnA0 = (Button)sender;
			picMine.Image = btnA0.Image;
			
			if (btnA0 == btnS)
			{
				player.InMind = SRPGame.SRP.S;
			}
			else if (btnA0 == btnR)
			{
				player.InMind = SRPGame.SRP.R;
			}
			else if (btnA0 == btnP)
			{
				player.InMind = SRPGame.SRP.P;
			}
			else
			{
				throw new InvalidOperationException();
			}

			switch (computer.ForeseeInMind(player.InMind, 3, 1, 1))
			{
				case SRPGame.SRP.S:
					picCom.Image = Properties.Resources.gawei;
					break;
				case SRPGame.SRP.R:
					picCom.Image = Properties.Resources.bawei;
					break;
				case SRPGame.SRP.P:
					picCom.Image = Properties.Resources.bo;
					break;
				default:
					throw new InvalidOperationException();
			}
			
			switch (srpGame.NextMatch(new SRPGame.Match(player, computer)))
			{
				case -1:
					lblResult.Text = "내가 졌다";
					PrintLose(lblMyResult);
					PrintWin(lblComResult);
					break;
				case 0:
					lblResult.Text = "서로 비겼다";
					PrintDraw(lblMyResult);
					PrintDraw(lblComResult);
					break;
				case 1:
					lblResult.Text = "내가 이겼다";
					PrintWin(lblMyResult);
					PrintLose(lblComResult);
					break;
				default:
					throw new InvalidOperationException();
			}

			UpdateStat();
		}

		private void PrintWin(Control control)
		{
			PrintPersonalResult(control, "Win", Color.Cyan);
		}

		private void PrintLose(Control control)
		{
			PrintPersonalResult(control, "Lose", Color.Magenta);
		}

		private void PrintDraw(Control control)
		{
			PrintPersonalResult(control, "Draw", Color.Black);
		}

		private void PrintPersonalResult(Control control, string text, Color color)
		{
			control.Text = text;
			control.ForeColor = color;
		}

		private void UpdateStat()
		{
			lblMyWinCount.Text = srpGame.ScoreBoard[player][computer].Win.ToString();
			lblComWinCount.Text = srpGame.ScoreBoard[computer][player].Win.ToString();
			lblDrawCount.Text = srpGame.ScoreBoard[computer][player].Draw.ToString();
			lblMatchCount.Text = srpGame.ScoreBoard[computer][player].Match.ToString();

			lblMySCount.Text = player[SRPGame.SRP.S].ToString();
			lblMyRCount.Text = player[SRPGame.SRP.R].ToString();
			lblMyPCount.Text = player[SRPGame.SRP.P].ToString();

			lblComSCount.Text = computer[SRPGame.SRP.S].ToString();
			lblComRCount.Text = computer[SRPGame.SRP.R].ToString();
			lblComPCount.Text = computer[SRPGame.SRP.P].ToString();
		}

		private void BtnStat_Click(object sender, EventArgs e)
		{
			isStatViewShowing = !isStatViewShowing;
			timerStat.Start();
			if (isStatViewShowing)
			{
				btnStat.Text = "결과 접기 ◀";
			}
			else
			{
				btnStat.Text = "결과 보기 ▶";
			}
		}

		private void TimerStat_Tick(object sender, EventArgs e)
		{
			if (isStatViewShowing)
			{
				Width += widthChangePerTickEvent;
				if (Width < MaximumSize.Width) return;
			}
			else
			{
				Width -= widthChangePerTickEvent;
				if (MinimumSize.Width < Width) return;
			}

			timerStat.Stop();
		}
	}
}
