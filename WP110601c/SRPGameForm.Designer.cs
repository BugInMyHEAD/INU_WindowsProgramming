﻿namespace WP110601c
{
	partial class SRPGameForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.picCom = new System.Windows.Forms.PictureBox();
			this.picMine = new System.Windows.Forms.PictureBox();
			this.btnS = new System.Windows.Forms.Button();
			this.btnR = new System.Windows.Forms.Button();
			this.btnP = new System.Windows.Forms.Button();
			this.lblResult = new System.Windows.Forms.Label();
			this.lblMyResult = new System.Windows.Forms.Label();
			this.lblComResult = new System.Windows.Forms.Label();
			this.btnStat = new System.Windows.Forms.Button();
			this.grbStat = new System.Windows.Forms.GroupBox();
			this.lblComPCount = new System.Windows.Forms.Label();
			this.lblMyPCount = new System.Windows.Forms.Label();
			this.lblComRCount = new System.Windows.Forms.Label();
			this.lblMyRCount = new System.Windows.Forms.Label();
			this.lblComSCount = new System.Windows.Forms.Label();
			this.lblMySCount = new System.Windows.Forms.Label();
			this.lblMatchCount = new System.Windows.Forms.Label();
			this.lblDrawCount = new System.Windows.Forms.Label();
			this.lblComWinCount = new System.Windows.Forms.Label();
			this.lblMyWinCount = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.lblMe = new System.Windows.Forms.Label();
			this.lblComP = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lblComR = new System.Windows.Forms.Label();
			this.lblMyR = new System.Windows.Forms.Label();
			this.lblComS = new System.Windows.Forms.Label();
			this.lblMyS = new System.Windows.Forms.Label();
			this.lblMatch = new System.Windows.Forms.Label();
			this.lblDraw = new System.Windows.Forms.Label();
			this.lblComWin = new System.Windows.Forms.Label();
			this.lblMyWin = new System.Windows.Forms.Label();
			this.timerStat = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.picCom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picMine)).BeginInit();
			this.grbStat.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(42, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(17, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "컴";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(42, 102);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(17, 12);
			this.label2.TabIndex = 0;
			this.label2.Text = "나";
			// 
			// picCom
			// 
			this.picCom.Location = new System.Drawing.Point(65, 27);
			this.picCom.Name = "picCom";
			this.picCom.Size = new System.Drawing.Size(100, 50);
			this.picCom.TabIndex = 1;
			this.picCom.TabStop = false;
			// 
			// picMine
			// 
			this.picMine.Location = new System.Drawing.Point(65, 83);
			this.picMine.Name = "picMine";
			this.picMine.Size = new System.Drawing.Size(100, 50);
			this.picMine.TabIndex = 1;
			this.picMine.TabStop = false;
			// 
			// btnS
			// 
			this.btnS.Image = global::WP110601c.Properties.Resources.gawei;
			this.btnS.Location = new System.Drawing.Point(12, 173);
			this.btnS.Name = "btnS";
			this.btnS.Size = new System.Drawing.Size(80, 61);
			this.btnS.TabIndex = 2;
			this.btnS.Text = "가위";
			this.btnS.UseVisualStyleBackColor = true;
			this.btnS.Click += new System.EventHandler(this.BtnSRP_Click);
			// 
			// btnR
			// 
			this.btnR.Image = global::WP110601c.Properties.Resources.bawei;
			this.btnR.Location = new System.Drawing.Point(98, 173);
			this.btnR.Name = "btnR";
			this.btnR.Size = new System.Drawing.Size(83, 61);
			this.btnR.TabIndex = 2;
			this.btnR.Text = "바위";
			this.btnR.UseVisualStyleBackColor = true;
			this.btnR.Click += new System.EventHandler(this.BtnSRP_Click);
			// 
			// btnP
			// 
			this.btnP.Image = global::WP110601c.Properties.Resources.bo;
			this.btnP.Location = new System.Drawing.Point(187, 173);
			this.btnP.Name = "btnP";
			this.btnP.Size = new System.Drawing.Size(85, 61);
			this.btnP.TabIndex = 2;
			this.btnP.Text = "보";
			this.btnP.UseVisualStyleBackColor = true;
			this.btnP.Click += new System.EventHandler(this.BtnSRP_Click);
			// 
			// lblResult
			// 
			this.lblResult.AutoSize = true;
			this.lblResult.Location = new System.Drawing.Point(12, 149);
			this.lblResult.Name = "lblResult";
			this.lblResult.Size = new System.Drawing.Size(9, 12);
			this.lblResult.TabIndex = 3;
			this.lblResult.Text = " ";
			// 
			// lblMyResult
			// 
			this.lblMyResult.AutoSize = true;
			this.lblMyResult.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblMyResult.Location = new System.Drawing.Point(197, 102);
			this.lblMyResult.Name = "lblMyResult";
			this.lblMyResult.Size = new System.Drawing.Size(19, 24);
			this.lblMyResult.TabIndex = 3;
			this.lblMyResult.Text = " ";
			// 
			// lblComResult
			// 
			this.lblComResult.AutoSize = true;
			this.lblComResult.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblComResult.Location = new System.Drawing.Point(197, 48);
			this.lblComResult.Name = "lblComResult";
			this.lblComResult.Size = new System.Drawing.Size(19, 24);
			this.lblComResult.TabIndex = 3;
			this.lblComResult.Text = " ";
			// 
			// btnStat
			// 
			this.btnStat.Location = new System.Drawing.Point(187, 134);
			this.btnStat.Name = "btnStat";
			this.btnStat.Size = new System.Drawing.Size(86, 27);
			this.btnStat.TabIndex = 4;
			this.btnStat.UseVisualStyleBackColor = true;
			this.btnStat.Click += new System.EventHandler(this.BtnStat_Click);
			// 
			// grbStat
			// 
			this.grbStat.Controls.Add(this.lblComPCount);
			this.grbStat.Controls.Add(this.lblMyPCount);
			this.grbStat.Controls.Add(this.lblComRCount);
			this.grbStat.Controls.Add(this.lblMyRCount);
			this.grbStat.Controls.Add(this.lblComSCount);
			this.grbStat.Controls.Add(this.lblMySCount);
			this.grbStat.Controls.Add(this.lblMatchCount);
			this.grbStat.Controls.Add(this.lblDrawCount);
			this.grbStat.Controls.Add(this.lblComWinCount);
			this.grbStat.Controls.Add(this.lblMyWinCount);
			this.grbStat.Controls.Add(this.label7);
			this.grbStat.Controls.Add(this.lblMe);
			this.grbStat.Controls.Add(this.lblComP);
			this.grbStat.Controls.Add(this.label5);
			this.grbStat.Controls.Add(this.lblComR);
			this.grbStat.Controls.Add(this.lblMyR);
			this.grbStat.Controls.Add(this.lblComS);
			this.grbStat.Controls.Add(this.lblMyS);
			this.grbStat.Controls.Add(this.lblMatch);
			this.grbStat.Controls.Add(this.lblDraw);
			this.grbStat.Controls.Add(this.lblComWin);
			this.grbStat.Controls.Add(this.lblMyWin);
			this.grbStat.Location = new System.Drawing.Point(300, 23);
			this.grbStat.Name = "grbStat";
			this.grbStat.Size = new System.Drawing.Size(264, 210);
			this.grbStat.TabIndex = 5;
			this.grbStat.TabStop = false;
			this.grbStat.Text = "게임 통계";
			// 
			// lblComPCount
			// 
			this.lblComPCount.AutoSize = true;
			this.lblComPCount.Location = new System.Drawing.Point(208, 174);
			this.lblComPCount.Name = "lblComPCount";
			this.lblComPCount.Size = new System.Drawing.Size(9, 12);
			this.lblComPCount.TabIndex = 1;
			this.lblComPCount.Text = " ";
			// 
			// lblMyPCount
			// 
			this.lblMyPCount.AutoSize = true;
			this.lblMyPCount.Location = new System.Drawing.Point(69, 174);
			this.lblMyPCount.Name = "lblMyPCount";
			this.lblMyPCount.Size = new System.Drawing.Size(9, 12);
			this.lblMyPCount.TabIndex = 1;
			this.lblMyPCount.Text = " ";
			// 
			// lblComRCount
			// 
			this.lblComRCount.AutoSize = true;
			this.lblComRCount.Location = new System.Drawing.Point(208, 150);
			this.lblComRCount.Name = "lblComRCount";
			this.lblComRCount.Size = new System.Drawing.Size(9, 12);
			this.lblComRCount.TabIndex = 1;
			this.lblComRCount.Text = " ";
			// 
			// lblMyRCount
			// 
			this.lblMyRCount.AutoSize = true;
			this.lblMyRCount.Location = new System.Drawing.Point(69, 150);
			this.lblMyRCount.Name = "lblMyRCount";
			this.lblMyRCount.Size = new System.Drawing.Size(9, 12);
			this.lblMyRCount.TabIndex = 1;
			this.lblMyRCount.Text = " ";
			// 
			// lblComSCount
			// 
			this.lblComSCount.AutoSize = true;
			this.lblComSCount.Location = new System.Drawing.Point(208, 135);
			this.lblComSCount.Name = "lblComSCount";
			this.lblComSCount.Size = new System.Drawing.Size(9, 12);
			this.lblComSCount.TabIndex = 1;
			this.lblComSCount.Text = " ";
			// 
			// lblMySCount
			// 
			this.lblMySCount.AutoSize = true;
			this.lblMySCount.Location = new System.Drawing.Point(69, 135);
			this.lblMySCount.Name = "lblMySCount";
			this.lblMySCount.Size = new System.Drawing.Size(9, 12);
			this.lblMySCount.TabIndex = 1;
			this.lblMySCount.Text = " ";
			// 
			// lblMatchCount
			// 
			this.lblMatchCount.AutoSize = true;
			this.lblMatchCount.Location = new System.Drawing.Point(130, 79);
			this.lblMatchCount.Name = "lblMatchCount";
			this.lblMatchCount.Size = new System.Drawing.Size(9, 12);
			this.lblMatchCount.TabIndex = 1;
			this.lblMatchCount.Text = " ";
			// 
			// lblDrawCount
			// 
			this.lblDrawCount.AutoSize = true;
			this.lblDrawCount.Location = new System.Drawing.Point(130, 60);
			this.lblDrawCount.Name = "lblDrawCount";
			this.lblDrawCount.Size = new System.Drawing.Size(9, 12);
			this.lblDrawCount.TabIndex = 1;
			this.lblDrawCount.Text = " ";
			// 
			// lblComWinCount
			// 
			this.lblComWinCount.AutoSize = true;
			this.lblComWinCount.Location = new System.Drawing.Point(130, 42);
			this.lblComWinCount.Name = "lblComWinCount";
			this.lblComWinCount.Size = new System.Drawing.Size(9, 12);
			this.lblComWinCount.TabIndex = 1;
			this.lblComWinCount.Text = " ";
			// 
			// lblMyWinCount
			// 
			this.lblMyWinCount.AutoSize = true;
			this.lblMyWinCount.Location = new System.Drawing.Point(130, 24);
			this.lblMyWinCount.Name = "lblMyWinCount";
			this.lblMyWinCount.Size = new System.Drawing.Size(9, 12);
			this.lblMyWinCount.TabIndex = 1;
			this.lblMyWinCount.Text = " ";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(177, 111);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(17, 12);
			this.label7.TabIndex = 0;
			this.label7.Text = "컴";
			// 
			// lblMe
			// 
			this.lblMe.AutoSize = true;
			this.lblMe.Location = new System.Drawing.Point(38, 111);
			this.lblMe.Name = "lblMe";
			this.lblMe.Size = new System.Drawing.Size(17, 12);
			this.lblMe.TabIndex = 0;
			this.lblMe.Text = "나";
			// 
			// lblComP
			// 
			this.lblComP.AutoSize = true;
			this.lblComP.Location = new System.Drawing.Point(146, 174);
			this.lblComP.Name = "lblComP";
			this.lblComP.Size = new System.Drawing.Size(45, 12);
			this.lblComP.TabIndex = 0;
			this.lblComP.Text = "보 횟수";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 174);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(45, 12);
			this.label5.TabIndex = 0;
			this.label5.Text = "보 횟수";
			// 
			// lblComR
			// 
			this.lblComR.AutoSize = true;
			this.lblComR.Location = new System.Drawing.Point(145, 150);
			this.lblComR.Name = "lblComR";
			this.lblComR.Size = new System.Drawing.Size(57, 12);
			this.lblComR.TabIndex = 0;
			this.lblComR.Text = "바위 횟수";
			// 
			// lblMyR
			// 
			this.lblMyR.AutoSize = true;
			this.lblMyR.Location = new System.Drawing.Point(6, 150);
			this.lblMyR.Name = "lblMyR";
			this.lblMyR.Size = new System.Drawing.Size(57, 12);
			this.lblMyR.TabIndex = 0;
			this.lblMyR.Text = "바위 횟수";
			// 
			// lblComS
			// 
			this.lblComS.AutoSize = true;
			this.lblComS.Location = new System.Drawing.Point(145, 135);
			this.lblComS.Name = "lblComS";
			this.lblComS.Size = new System.Drawing.Size(57, 12);
			this.lblComS.TabIndex = 0;
			this.lblComS.Text = "가위 횟수";
			// 
			// lblMyS
			// 
			this.lblMyS.AutoSize = true;
			this.lblMyS.Location = new System.Drawing.Point(6, 135);
			this.lblMyS.Name = "lblMyS";
			this.lblMyS.Size = new System.Drawing.Size(57, 12);
			this.lblMyS.TabIndex = 0;
			this.lblMyS.Text = "가위 횟수";
			// 
			// lblMatch
			// 
			this.lblMatch.AutoSize = true;
			this.lblMatch.Location = new System.Drawing.Point(7, 79);
			this.lblMatch.Name = "lblMatch";
			this.lblMatch.Size = new System.Drawing.Size(73, 12);
			this.lblMatch.TabIndex = 0;
			this.lblMatch.Text = "총 게임 횟수";
			// 
			// lblDraw
			// 
			this.lblDraw.AutoSize = true;
			this.lblDraw.Location = new System.Drawing.Point(7, 60);
			this.lblDraw.Name = "lblDraw";
			this.lblDraw.Size = new System.Drawing.Size(57, 12);
			this.lblDraw.TabIndex = 0;
			this.lblDraw.Text = "비긴 횟수";
			// 
			// lblComWin
			// 
			this.lblComWin.AutoSize = true;
			this.lblComWin.Location = new System.Drawing.Point(7, 42);
			this.lblComWin.Name = "lblComWin";
			this.lblComWin.Size = new System.Drawing.Size(85, 12);
			this.lblComWin.TabIndex = 0;
			this.lblComWin.Text = "컴이 이긴 횟수";
			// 
			// lblMyWin
			// 
			this.lblMyWin.AutoSize = true;
			this.lblMyWin.Location = new System.Drawing.Point(7, 25);
			this.lblMyWin.Name = "lblMyWin";
			this.lblMyWin.Size = new System.Drawing.Size(85, 12);
			this.lblMyWin.TabIndex = 0;
			this.lblMyWin.Text = "내가 이긴 횟수";
			// 
			// timerStat
			// 
			this.timerStat.Interval = 1;
			this.timerStat.Tick += new System.EventHandler(this.TimerStat_Tick);
			// 
			// SRPGameForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.grbStat);
			this.Controls.Add(this.btnStat);
			this.Controls.Add(this.lblComResult);
			this.Controls.Add(this.lblMyResult);
			this.Controls.Add(this.lblResult);
			this.Controls.Add(this.btnP);
			this.Controls.Add(this.btnR);
			this.Controls.Add(this.btnS);
			this.Controls.Add(this.picMine);
			this.Controls.Add(this.picCom);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(600, 300);
			this.MinimumSize = new System.Drawing.Size(300, 300);
			this.Name = "SRPGameForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "가위바위보 게임";
			((System.ComponentModel.ISupportInitialize)(this.picCom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picMine)).EndInit();
			this.grbStat.ResumeLayout(false);
			this.grbStat.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PictureBox picCom;
		private System.Windows.Forms.PictureBox picMine;
		private System.Windows.Forms.Button btnS;
		private System.Windows.Forms.Button btnR;
		private System.Windows.Forms.Button btnP;
		private System.Windows.Forms.Label lblResult;
		private System.Windows.Forms.Label lblMyResult;
		private System.Windows.Forms.Label lblComResult;
		private System.Windows.Forms.Button btnStat;
		private System.Windows.Forms.GroupBox grbStat;
		private System.Windows.Forms.Label lblMe;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lblMyR;
		private System.Windows.Forms.Label lblMyS;
		private System.Windows.Forms.Label lblMatch;
		private System.Windows.Forms.Label lblDraw;
		private System.Windows.Forms.Label lblComWin;
		private System.Windows.Forms.Label lblMyWin;
		private System.Windows.Forms.Label lblMyPCount;
		private System.Windows.Forms.Label lblMyRCount;
		private System.Windows.Forms.Label lblMySCount;
		private System.Windows.Forms.Label lblMatchCount;
		private System.Windows.Forms.Label lblDrawCount;
		private System.Windows.Forms.Label lblComWinCount;
		private System.Windows.Forms.Label lblMyWinCount;
		private System.Windows.Forms.Label lblComPCount;
		private System.Windows.Forms.Label lblComRCount;
		private System.Windows.Forms.Label lblComSCount;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label lblComP;
		private System.Windows.Forms.Label lblComR;
		private System.Windows.Forms.Label lblComS;
		private System.Windows.Forms.Timer timerStat;
	}
}

