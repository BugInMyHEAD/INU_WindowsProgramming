﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP101602h
{
	class Method_Class
	{
		static void Main()
		{
			Console.Write("하나의 수 입력 : ");
			Top.a = Convert.ToInt16(Console.ReadLine());  //3
			Console.Write("또 하나의 수 입력 : ");
			Top.b = Convert.ToInt16(Console.ReadLine());//6
			int sum = Top.Hap();

			ABC.M_D sbs = new ABC.M_D
			{
				c = Top.a,
				d = Top.b
			};
			int r_multi = sbs.Multi();

			Console.WriteLine("{0} + {1} = {2}", Top.a, Top.b, sum);
			Console.WriteLine("{0} * {1} = {2}", sbs.c, sbs.d, r_multi);
		}
	}
}
