﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP110102c
{
	public partial class 과제4 : Form
	{
		public 과제4()
		{
			InitializeComponent();
		}

		private void BtnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void BtnLogin_Click(object sender, EventArgs e)
		{
			btnInquiry.Enabled = false;
			btnGrant.Enabled = false;
			switch (LoginCheck())
			{
				case LoginTry.IDEMPTY:
					MessageBox.Show("로그인 아이디를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtId.Focus();
					break;
				case LoginTry.IDNOTFOUND:
					lblState.Text = "일치하는 아이디가 없습니다.";
					txtId.Clear();
					txtId.Focus();
					break;
				case LoginTry.PWEMPTY:
					MessageBox.Show("로그인 비밀번호를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtPassword.Focus();
					break;
				case LoginTry.PWINCORRECT:
					lblState.Text = "비밀번호가 틀렸습니다.";
					txtPassword.Clear();
					txtPassword.Focus();
					break;
				case LoginTry.SUCCESS:
					lblState.Text = "관리자님 환영합니다.";
					btnInquiry.Enabled = true;
					btnGrant.Enabled = true;
					break;
				default:
					throw new InvalidOperationException();
			}
		}

		private LoginTry LoginCheck()
		{
			if (txtId.Text == "")
			{
				return LoginTry.IDEMPTY;
			}
			else if (txtId.Text == "test")
			{
				if (txtPassword.Text == "")
				{
					return LoginTry.PWEMPTY;
				}
				else if (txtPassword.Text == "1234")
				{
					return LoginTry.SUCCESS;
				}
				else
				{
					return LoginTry.PWINCORRECT;
				}
			}
			else
			{
				return LoginTry.IDNOTFOUND;
			}
		}

		private void TxtPassword_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				BtnLogin_Click(sender, e);
			}
		}

		enum LoginTry
		{
			IDEMPTY, IDNOTFOUND, PWEMPTY, PWINCORRECT, SUCCESS
		}
	}
}
