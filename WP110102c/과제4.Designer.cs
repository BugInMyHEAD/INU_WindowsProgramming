﻿namespace WP110102c
{
	partial class 과제4
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbiId = new System.Windows.Forms.Label();
			this.lblPassword = new System.Windows.Forms.Label();
			this.txtId = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.btnInquiry = new System.Windows.Forms.Button();
			this.btnGrant = new System.Windows.Forms.Button();
			this.btnBbs = new System.Windows.Forms.Button();
			this.btnVisitLog = new System.Windows.Forms.Button();
			this.btnLogin = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.lblState = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lbiId
			// 
			this.lbiId.AutoSize = true;
			this.lbiId.Location = new System.Drawing.Point(30, 50);
			this.lbiId.Name = "lbiId";
			this.lbiId.Size = new System.Drawing.Size(16, 12);
			this.lbiId.TabIndex = 0;
			this.lbiId.Text = "ID";
			// 
			// lblPassword
			// 
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(30, 84);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(62, 12);
			this.lblPassword.TabIndex = 0;
			this.lblPassword.Text = "Password";
			// 
			// txtId
			// 
			this.txtId.Location = new System.Drawing.Point(98, 47);
			this.txtId.MaxLength = 8;
			this.txtId.Name = "txtId";
			this.txtId.Size = new System.Drawing.Size(134, 21);
			this.txtId.TabIndex = 1;
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(98, 81);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new System.Drawing.Size(134, 21);
			this.txtPassword.TabIndex = 1;
			this.txtPassword.UseSystemPasswordChar = true;
			this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtPassword_KeyDown);
			// 
			// btnInquiry
			// 
			this.btnInquiry.Enabled = false;
			this.btnInquiry.Location = new System.Drawing.Point(47, 188);
			this.btnInquiry.Name = "btnInquiry";
			this.btnInquiry.Size = new System.Drawing.Size(85, 24);
			this.btnInquiry.TabIndex = 2;
			this.btnInquiry.Text = "회원조회";
			this.btnInquiry.UseVisualStyleBackColor = true;
			// 
			// btnGrant
			// 
			this.btnGrant.Enabled = false;
			this.btnGrant.Location = new System.Drawing.Point(47, 227);
			this.btnGrant.Name = "btnGrant";
			this.btnGrant.Size = new System.Drawing.Size(85, 25);
			this.btnGrant.TabIndex = 2;
			this.btnGrant.Text = "권한부여";
			this.btnGrant.UseVisualStyleBackColor = true;
			// 
			// btnBbs
			// 
			this.btnBbs.Location = new System.Drawing.Point(158, 188);
			this.btnBbs.Name = "btnBbs";
			this.btnBbs.Size = new System.Drawing.Size(85, 24);
			this.btnBbs.TabIndex = 2;
			this.btnBbs.Text = "게시판";
			this.btnBbs.UseVisualStyleBackColor = true;
			// 
			// btnVisitLog
			// 
			this.btnVisitLog.Location = new System.Drawing.Point(158, 227);
			this.btnVisitLog.Name = "btnVisitLog";
			this.btnVisitLog.Size = new System.Drawing.Size(85, 25);
			this.btnVisitLog.TabIndex = 2;
			this.btnVisitLog.Text = "방명록";
			this.btnVisitLog.UseVisualStyleBackColor = true;
			// 
			// btnLogin
			// 
			this.btnLogin.Location = new System.Drawing.Point(47, 119);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(85, 24);
			this.btnLogin.TabIndex = 2;
			this.btnLogin.Text = "로그인";
			this.btnLogin.UseVisualStyleBackColor = true;
			this.btnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(158, 119);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(85, 24);
			this.btnClose.TabIndex = 2;
			this.btnClose.Text = "닫기";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
			// 
			// lblState
			// 
			this.lblState.AutoSize = true;
			this.lblState.Location = new System.Drawing.Point(43, 159);
			this.lblState.MinimumSize = new System.Drawing.Size(200, 0);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(200, 12);
			this.lblState.TabIndex = 3;
			this.lblState.Text = "로그인해주세요";
			this.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// 과제4
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.lblState);
			this.Controls.Add(this.btnVisitLog);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnBbs);
			this.Controls.Add(this.btnLogin);
			this.Controls.Add(this.btnGrant);
			this.Controls.Add(this.btnInquiry);
			this.Controls.Add(this.txtPassword);
			this.Controls.Add(this.txtId);
			this.Controls.Add(this.lblPassword);
			this.Controls.Add(this.lbiId);
			this.Name = "과제4";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lbiId;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.TextBox txtId;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Button btnInquiry;
		private System.Windows.Forms.Button btnGrant;
		private System.Windows.Forms.Button btnBbs;
		private System.Windows.Forms.Button btnVisitLog;
		private System.Windows.Forms.Button btnLogin;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Label lblState;
	}
}

