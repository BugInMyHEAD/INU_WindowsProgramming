﻿namespace WP112004c
{
	partial class FrmComboBox
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblBirthYear = new System.Windows.Forms.Label();
			this.txtBirthYear = new System.Windows.Forms.TextBox();
			this.cmbYear = new System.Windows.Forms.ComboBox();
			this.btnSelect = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblBirthYear
			// 
			this.lblBirthYear.AutoSize = true;
			this.lblBirthYear.Location = new System.Drawing.Point(47, 42);
			this.lblBirthYear.Name = "lblBirthYear";
			this.lblBirthYear.Size = new System.Drawing.Size(53, 12);
			this.lblBirthYear.TabIndex = 0;
			this.lblBirthYear.Text = "출생년도";
			// 
			// txtBirthYear
			// 
			this.txtBirthYear.Location = new System.Drawing.Point(117, 39);
			this.txtBirthYear.Name = "txtBirthYear";
			this.txtBirthYear.Size = new System.Drawing.Size(100, 21);
			this.txtBirthYear.TabIndex = 1;
			// 
			// cmbYear
			// 
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Items.AddRange(new object[] {
            "2001",
            "2002",
            "2003",
            "2004",
            "2005"});
			this.cmbYear.Location = new System.Drawing.Point(33, 122);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(121, 20);
			this.cmbYear.TabIndex = 2;
			// 
			// btnSelect
			// 
			this.btnSelect.Location = new System.Drawing.Point(172, 122);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(75, 23);
			this.btnSelect.TabIndex = 3;
			this.btnSelect.Text = "선택";
			this.btnSelect.UseVisualStyleBackColor = true;
			// 
			// FrmComboBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.btnSelect);
			this.Controls.Add(this.cmbYear);
			this.Controls.Add(this.txtBirthYear);
			this.Controls.Add(this.lblBirthYear);
			this.Name = "FrmComboBox";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblBirthYear;
		private System.Windows.Forms.TextBox txtBirthYear;
		private System.Windows.Forms.ComboBox cmbYear;
		private System.Windows.Forms.Button btnSelect;
	}
}

