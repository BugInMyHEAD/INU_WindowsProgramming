﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcException : Exception
	{
		public CalcException() : base() { }

		public CalcException(string message) : base(message) { }
	}
}
