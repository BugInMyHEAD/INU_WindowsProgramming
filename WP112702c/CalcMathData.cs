﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public abstract class CalcMathData : CalcData
	{
		public CalcMathData(string text) : base(text) { }

		public override string ToString() => base.ToString();
	}
}
