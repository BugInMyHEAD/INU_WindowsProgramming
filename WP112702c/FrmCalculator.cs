﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP112702c
{
	public partial class FrmCalculator : Form
	{
		Calculator calculator;

		public FrmCalculator()
		{
			InitializeComponent();

			calculator = new Calculator(new CalcDisplay(calcDisplay));
		}

		class CalcDisplay : ICalcDisplay
		{
			Control expression;
			LinkedList<string> stored = new LinkedList<string>();

			public CalcDisplay(Control expression)
			{
				this.expression = expression;
			}

			public void Input(CalcData data)
			{
				stored.AddLast(expression.Text);
				if (data is CalcResult || data is CalcMessage)
				{
					expression.Text += "=(" + data + ")";
				}
				else
				{
					expression.Text += data;
				}
			}

			public void Clr()
			{
				stored.Clear();
				expression.Text = null;
			}

			public void Del()
			{
				expression.Text = stored.LastOrDefault();
				try
				{
					stored.RemoveLast();
				}
				catch (InvalidOperationException) { }
			}
		}

		private void CalcDigitButton_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Digit[int.Parse(((Button)sender).Text, System.Globalization.NumberStyles.HexNumber)]);
		}

		private void CalcButtonClr_Click(object sender, EventArgs e)
		{
			calculator.Clr();
		}

		private void CalcButtonAns_Click(object sender, EventArgs e)
		{
			calculator.Ans();
		}

		private void CalcButtonAdd_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Add);
		}

		private void CalcButtonSub_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Sub);
		}

		private void CalcButtonMul_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Mul);
		}

		private void CalcButtonDiv_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Div);
		}

		private void CalcButtonMod_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Mod);
		}

		private void CalcButtonPow_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Pow);
		}

		private void CalcButtonFact_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Factopr);
		}

		private void CalcButtonSqrt_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Sqrt);
		}

		private void CalcButtonDel_Click(object sender, EventArgs e)
		{
			calculator.Del();
		}

		private void CalcButtonCe_Click(object sender, EventArgs e)
		{
			calculator.Ce();
		}

		private void CalcButtonPi_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Pi);
		}

		private void CalcButtonDot_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Dot);
		}

		private void CalcButtonE_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.E);
		}

		private void CalcButtonParopen_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Paropen);
		}

		private void CalcButtonParclse_Click(object sender, EventArgs e)
		{
			calculator.Input(calculator.Parclose);
		}
	}
}
