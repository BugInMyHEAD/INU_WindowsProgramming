﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public interface ICalcDisplay
	{
		void Input(CalcData data);
		void Clr();
		void Del();
	}
}
