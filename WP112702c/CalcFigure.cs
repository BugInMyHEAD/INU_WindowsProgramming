﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcFigure : CalcMathData, ICalcValue
	{
		public double Value { get; }

		public CalcFigure(double value) : this(value, null) { }

		public CalcFigure(double value, string text) : base(text)
		{
			Value = value;
		}

		public override string ToString()
		{
			string text = base.ToString();
			if (null == text)
			{
				return Value.ToString();
			}

			return text;
		}
	}
}
