﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcDisplay : ICalcDisplay
	{
		public static readonly ICalcDisplay NoDisplay = new CalcDisplay();

		private CalcDisplay() { }

		public void Clr() { }
		public void Del() { }
		public void Input(CalcData data) { }
	}
}
