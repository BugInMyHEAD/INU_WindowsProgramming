﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcWrongExpressionException : CalcException
	{
		public CalcWrongExpressionException() : base() { }

		public CalcWrongExpressionException(string message) : base(message) { }
	}
}
