﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcResult : CalcMathData, ICalcValue
	{
		public CalcFigure CalcFigure { get; }

		public double Value => CalcFigure.Value;

		public CalcResult(double value) : base(null)
		{
			CalcFigure = new CalcFigure(value);
		}

		public override string ToString()
		{
			return CalcFigure.ToString();
		}
	}
}
