﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class Calculator : CalcCore
	{
		ICalcDisplay display;
		internal LinkedList<CalcMathInput> InputDeque { get; } = new LinkedList<CalcMathInput>();
		bool calcExceptionOccured;
		//	ArrayDeque<Integer> nextargDeque = new ArrayDeque<>(); // TODO
		//Func<CalcMathInput> AnsFunc => null;
		
		public Calculator(ICalcDisplay display = null)
		{
			if (null == display)
			{
				this.display = CalcDisplay.NoDisplay;
			}
			else
			{
				this.display = display;
			}
			ClrHelper();
		}

		public override bool CanInput(CalcData data)
		{
			return !calcExceptionOccured && data is CalcMathData mdata && null != MakeInput(mdata);
		}

		protected bool NeedOpdBefore(CalcMathData mdata)
		{
			return mdata is CalcInfixOpr && !mdata.Equals(Paropen) || mdata.Equals(Nextarg) || mdata is CalcPostfixUnaryOpr;
		}

		protected bool NeedOpdAfter(CalcMathData mdata)
		{
			return mdata is CalcArithmetic && !( mdata.Equals(Parclose) || mdata is CalcPostfixUnaryOpr );
		}

		private CalcMathInput MakeInput(CalcMathData mdata)
		{
			CalcMathInput last = InputDeque.LastOrDefault();

			if (default(CalcMathInput) == last)
			{
				if (!NeedOpdBefore(mdata))
				{
					InputDeque.AddLast(new CalcMathInput(this, Digit[0], CalcDigitInfo.Initial0));
					return MakeInput(mdata);
				}

				return null;
			}

			/*if (last.Data is CalcAnswer)
			{
				return AnsFunc();
			}*/

			if (mdata is CalcDigit dmdata)
			{
				if (last.Data is CalcFigure || last.Data is CalcResult || last.Data.Equals(Parclose))
				{
					return null;
				}
				if (dmdata.IsPoint)
				{
					if (last.CalcDigitInfo != CalcDigitInfo.Decimal)
					{
						return new CalcMathInput(this, mdata, CalcDigitInfo.Decimal);
					}
				}
				else if (last.CalcDigitInfo != CalcDigitInfo.Initial0)
				{
					return new CalcMathInput(this, mdata, last.CalcDigitInfo);
				}
				else
				{
					switch (( dmdata.IsZero ? 1 << 1 : 0 ) | ( last.Data is CalcDigit ? 1 : 0 ))
					{
					// It's inferred that if (last.data is CalcDigit) is true then (last.data) is digit[0].
					case 0b00:
						return new CalcMathInput(this, mdata, CalcDigitInfo.Integer);
					case 0b01:
						DelHelper();
						return new CalcMathInput(this, mdata, CalcDigitInfo.Integer);
					case 0b10:
						return new CalcMathInput(this, mdata, CalcDigitInfo.Initial0);
					case 0b11:
						break;
					default:
						throw new ApplicationException();
					}
				}

				return null;
			}

			if (mdata is CalcArithmetic)
			{
				if (mdata.Equals(Nextarg))
				{
					if (NeedOpdAfter(last.Data))
					{
						return null;
					}
				}
				else if (mdata.Equals(Paropen))
				{
					if (!NeedOpdAfter(last.Data))
					{
						if (CalcDigitInfo.Initial0 == last.CalcDigitInfo)
						{
							DelHelper();
						}
						else
						{
							return null;
						}
					}
				}
				else if (mdata.Equals(Parclose))
				{
					if (0 == last.ParcloseDemand || NeedOpdAfter(last.Data))
					{
						return null;
					}
				}
				else if (mdata is CalcInfixOpr)
				{
					if (NeedOpdAfter(last.Data))
					{
						return null;
					}
				}
				else if (mdata is CalcMathFunc)
				{
					if (last.Data is CalcDigit)
					{
						if (last.CalcDigitInfo != CalcDigitInfo.Initial0)
						{
							return null;
						}
						DelHelper();
					}
					else if (!NeedOpdAfter(last.Data))
					{
						return null;
					}
				}
				else if (mdata is CalcPostfixUnaryOpr)
				{
					if (NeedOpdAfter(last.Data))
					{
						return null;
					}
				}

				return new CalcMathInput(this, mdata, CalcDigitInfo.Initial0);
			}
			else if (mdata is CalcFigure)
			{
				if (last.Data is CalcDigit)
				{
					if (last.CalcDigitInfo != CalcDigitInfo.Initial0)
					{
						return null;
					}

					DelHelper();
				}
				else if (!NeedOpdAfter(last.Data))
				{
					return null;
				}

				return new CalcMathInput(this, mdata, CalcDigitInfo.Initial0);
			}
			else if (mdata is CalcResult)
			{
				return new CalcMathInput(this, mdata, CalcDigitInfo.Initial0);
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		public override bool Input(CalcData data)
		{
			try
			{
				var var2 = MakeInput((CalcMathData)data);
				if (null != var2)
				{
					InputDeque.AddLast(var2);
					display.Input(data);

					return true;
				}
			}
			catch (InvalidCastException) { }

			return false;
		}

		public override double Ans()
		{
			double answer = double.NaN; // I changed the initial value from null to NaN
			try
			{
				answer = ( (CalcResult)InputDeque.Last().Data ).Value;
			}
			catch (InvalidCastException)
			{
				try
				{
					answer = Calculate();
					Input(new CalcResult(answer));
				}
				catch (CalcWrongExpressionException excep3)
				{
					if (!calcExceptionOccured)
					{
						display.Input(new CalcMessage(excep3.Message));
						calcExceptionOccured = true;
					}
				}
			}

			return answer;
		}

		public void Clr()
		{
			ClrHelper();
		}

		private void ClrHelper()
		{
			InputDeque.Clear();
			calcExceptionOccured = false;
			display.Clr();
			InputDeque.AddLast(new CalcMathInput(this, Digit[0], CalcDigitInfo.Initial0));
			display.Input(Digit[0]);
		}

		public void Ce()
		{
			try
			{
				if (InputDeque.Last().Data is CalcDigit)
				{
					do
					{
						Del();
					} while (InputDeque.Last().Data is CalcDigit && ( !InputDeque.Last().CalcDigitInfo.Equals(CalcDigitInfo.Initial0) || InputDeque.Count > 1 ));
				}
				else
				{
					Del();
				}
			}
			catch (InvalidOperationException)
			{
				Clr();
			}
		}

		public void Del()
		{
			DelHelper();

			if (InputDeque.Count == 0)
			{
				Clr();
			}
		}

		private void DelHelper()
		{
			try
			{
				if (!calcExceptionOccured)
				{
					InputDeque.RemoveLast();
				}
			}
			catch (InvalidOperationException) { }

			calcExceptionOccured = false;
			display.Del();
		}

		public override IEnumerator<CalcMathData> GetEnumerator()
		{
			return new CalcMathInputIEAdapter(InputDeque.GetEnumerator());
		}

		public override IEnumerator<CalcMathData> GetEnumeratorOfExpr()
		{
			return new CalcMathInputIEAdapter(InputDeque.Skip(InputDeque.Last().AnsPos).GetEnumerator());
		}
	}

	class CalcMathInput
	{
		internal CalcMathData Data { get; }
		internal int AnsPos { get; }
		internal int ParcloseDemand { get; }
		internal CalcDigitInfo CalcDigitInfo { get; }

		internal CalcMathInput(Calculator calculator, CalcMathData mdata)
			: this(calculator, mdata, calculator.InputDeque.Last().CalcDigitInfo) { }

		internal CalcMathInput(Calculator calculator, CalcMathData mdata, CalcDigitInfo calcDigitInfo)
		{
			this.Data = mdata;
			this.CalcDigitInfo = calcDigitInfo;

			if (mdata.Equals(calculator.Paropen) || mdata is CalcMathFunc)
			{
				this.ParcloseDemand = 1;
			}
			else if (mdata.Equals(calculator.Parclose))
			{
				this.ParcloseDemand = -1;
			}

			try
			{
				if (mdata is CalcResult)
				{
					this.AnsPos = calculator.InputDeque.Count;
				}
				else
				{
					var last = calculator.InputDeque.Last();
					this.AnsPos = last.AnsPos;
					this.ParcloseDemand += last.ParcloseDemand;
				}
			}
			catch (InvalidOperationException)
			{
				// There isn't CalcAnswer, but this value is used to skip no elements in inputDeque.stream().skip(inputDeque.getLast().ansPos)
				this.AnsPos = 0;
			}

			if (this.ParcloseDemand < 0)
			{
				//throw new ApplicationException("ParclseDemand < 0");
			}
		}
	}

	class CalcMathInputIEAdapter : IEnumerator<CalcMathData>
	{
		IEnumerator<CalcMathInput> ie;

		public CalcMathInputIEAdapter(IEnumerator<CalcMathInput> ie)
		{
			this.ie = ie;
		}

		public CalcMathData Current => ie.Current.Data;
		object IEnumerator.Current => Current;

		public void Dispose() => ie.Dispose();
		public bool MoveNext() => ie.MoveNext();
		public void Reset() => ie.Reset();
	}

	enum CalcDigitInfo
	{
		Initial0, Integer, Decimal,
	}
}
