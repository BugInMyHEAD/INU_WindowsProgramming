﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcDigit : CalcMathData
	{
		/**
		 * 
		 * @param text used for displaying a decimal point. 
		 * @return {@code new CalcDigit(string, -1)}, which represents a decimal point.
		 * @see #CalcDigit(String, int) 
		 */
		public static CalcDigit DecimalPoint(string text)
		{
			return new CalcDigit(text, -1);
		}

		/**
		 * {@code this} represents a decimal point if {@code Value} is negative.
		 */
		public int Value { get; }

		/**
		 * @param digit displayed {@link String}
		 * @param value A digit's value.
		 */
		public CalcDigit(string digit, int value) : base(digit)
		{
			Value = value;
		}

		/**
		 * 
		 * @return Whether {@code this} is a decimal point.
		 */
		public bool IsPoint => Value < 0;

		/**
		 * 
		 * @return Whether {@code this} is a zero.
		 */
		public bool IsZero => 0 == Value;
	}
}
