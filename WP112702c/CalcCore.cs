﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public abstract class CalcCore : IEnumerable<CalcMathData>
	{
		protected static string wrongExpressionMessage = "Wrong expression";

		Stack<double> figureValueDeque = new Stack<double>();
		Stack<int> argCountDeque = new Stack<int>();

		public CalcMathData Paropen = new CalcInfixOpr("(", int.MaxValue, () => throw new CalcWrongExpressionException(wrongExpressionMessage));
		public CalcMathData Parclose = new CalcInfixOpr(")", int.MinValue, () => throw new CalcWrongExpressionException(wrongExpressionMessage));
		public CalcMathData Nextarg = new CalcArithmetic(",", () => throw new CalcWrongExpressionException(wrongExpressionMessage));

		class CalcMathFuncPar : CalcMathFunc
		{
			public const int Priority = int.MaxValue;
			public CalcMathFuncPar(string text, Action operation) : base(text, operation) { }

			public override string ToString()
			{
				return base.ToString() + "(" + ( 0 == maxArg ? ")" : "" );
			}
		}

		public CalcMathData Dot { get; } = CalcDigit.DecimalPoint(".");
		public CalcMathData[] Digit { get; } =
			{
				new CalcDigit("0", 0),
				new CalcDigit("1", 1),
				new CalcDigit("2", 2),
				new CalcDigit("3", 3),
				new CalcDigit("4", 4),
				new CalcDigit("5", 5),
				new CalcDigit("6", 6),
				new CalcDigit("7", 7),
				new CalcDigit("8", 8),
				new CalcDigit("9", 9)/*,
				new CalcDigit("A", 0xA),
				new CalcDigit("B", 0xB),
				new CalcDigit("C", 0xC),
				new CalcDigit("D", 0xD),
				new CalcDigit("E", 0xE),
				new CalcDigit("F", 0xF)*/
			};

		public CalcMathData Add { get; }
		public CalcMathData Sub { get; }
		public CalcMathData Mul { get; }
		public CalcMathData Pow { get; }
		public CalcMathData Div { get; }
		public CalcMathData Mod { get; }

		public CalcMathData Pi { get; } = new CalcFigure(Math.PI, "π");
		public CalcMathData E { get; } = new CalcFigure(Math.E, "e");

		public CalcMathData Sum { get; }
		public CalcMathData Factfunc { get; }
		public CalcMathData Factopr { get; }
		public CalcMathData Sqrt { get; }

		public CalcCore()
		{
			Add = new CalcInfixOpr("+", 3, delegate
				{
					try
					{
						figureValueDeque.Push(figureValueDeque.Pop() + figureValueDeque.Pop());
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Sub = new CalcInfixOpr("-", 3, delegate
				{
					try
					{
						double rightOpd = figureValueDeque.Pop();
						double leftOpd = figureValueDeque.Pop();
						figureValueDeque.Push(leftOpd - rightOpd);
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Mul = new CalcInfixOpr("*", 2, delegate
				{
					try
					{
						figureValueDeque.Push(figureValueDeque.Pop() * figureValueDeque.Pop());
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Pow = new CalcInfixOpr("^", 1, delegate
			{
				try
				{
					double rightOpd = figureValueDeque.Pop();
					double leftOpd = figureValueDeque.Pop();
					figureValueDeque.Push(Math.Pow(leftOpd, rightOpd));
				}
				catch (InvalidOperationException)
				{
					throw new CalcWrongExpressionException(wrongExpressionMessage);
				}
			});
			Div = new CalcInfixOpr("/", 2, delegate
				{
					try
					{
						double rightOpd = figureValueDeque.Pop();
						double leftOpd = figureValueDeque.Pop();
						figureValueDeque.Push(leftOpd / rightOpd);
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Mod = new CalcInfixOpr("%", 2, delegate
			{
				try
				{
					double rightOpd = figureValueDeque.Pop();
					double leftOpd = figureValueDeque.Pop();
					figureValueDeque.Push(leftOpd % rightOpd);
				}
				catch (InvalidOperationException)
				{
					throw new CalcWrongExpressionException(wrongExpressionMessage);
				}
			});

			Sum = new CalcMathFuncPar("sum", delegate
				{
					try
					{
						double sum;
						int argCount = argCountDeque.Pop();
						sum = 0.0;
						for (int i2 = 0; i2 < argCount; i2++)
						{
							sum += figureValueDeque.Pop();
						}
						figureValueDeque.Push(sum);
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Factfunc = new CalcMathFuncPar("fact", delegate
				{
					try
					{
						double fact, acc1;
						int argCount = argCountDeque.Pop();
						fact = 0.0;
						for (int i2 = 0; i2 < argCount; i2++)
						{
							fact += figureValueDeque.Pop();
						}
						acc1 = 1;
						for (int i2 = 1; i2 <= fact; i2++)
						{
							acc1 *= i2;
						}
						figureValueDeque.Push(acc1);
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Factopr = new CalcPostfixUnaryOpr("!", delegate
				{
					try
					{
						double fact, acc1;
						fact = figureValueDeque.Pop();
						acc1 = 1;
						for (int i2 = 1; i2 <= fact; i2++)
						{
							acc1 *= i2;
						}
						figureValueDeque.Push(acc1);
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
			Sqrt = new CalcMathFuncPar("√", delegate
				{
					try
					{
						double sqrt;
						int argCount = argCountDeque.Pop();
						sqrt = 0.0;
						for (int i2 = 0; i2 < argCount; i2++)
						{
							sqrt += figureValueDeque.Pop();
						}
						figureValueDeque.Push(Math.Sqrt(sqrt));
					}
					catch (InvalidOperationException)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
				});
		}

		public abstract IEnumerator<CalcMathData> GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();

		protected double Calculate()
		{
			var calcMathDataList = new List<CalcMathData>();
			ToPostfix(GetEnumeratorOfExpr(), calcMathDataList);

			figureValueDeque.Clear();
			foreach (var data in calcMathDataList)
			{
				try
				{
					figureValueDeque.Push(( (ICalcValue)data ).Value);
				}
				catch (InvalidCastException)
				{
					( (CalcArithmetic)data ).Operate();
				}
			}

			try
			{
				return figureValueDeque.Pop();
			}
			catch (InvalidOperationException)
			{
				throw new CalcWrongExpressionException(wrongExpressionMessage + figureValueDeque.Count);
			}
		}

		/**
		 * Also converts {@link CalcDigit} to {@link CalcFigure}.
		 * @param postfix in which the postfix notation will be written.
		 */
		private void ToPostfix(IEnumerator<CalcMathData> infix, ICollection<CalcMathData> postfix)
		{
			argCountDeque.Clear();
			var arithmeticDeque = new Stack<CalcArithmetic>();
			StringBuilder sbForDigit = new StringBuilder();

			while (infix.MoveNext())
			{
				CalcMathData data = infix.Current;

				try
				{
					sbForDigit.Append((CalcDigit)data);

					continue;
				}
				catch (InvalidCastException) { }

				try
				{
					CalcArithmetic arithmeticData = (CalcArithmetic)data;

					if (sbForDigit.Length > 0)
					{
						postfix.Add(new CalcFigure(double.Parse(sbForDigit.ToString())));
						sbForDigit.Clear();
					}

					if (arithmeticData.Equals(Parclose))
					{
						while (!( arithmeticDeque.Peek() is CalcMathFunc || arithmeticDeque.Peek().Equals(Paropen) ))
						{
							postfix.Add(arithmeticDeque.Pop());
						}
						if (arithmeticDeque.Peek() is CalcMathFunc)
						{
							postfix.Add(arithmeticDeque.Pop());
						}
						else
						{
							arithmeticDeque.Pop();
						}

						continue;
					}
					if (arithmeticData.Equals(Nextarg))
					{
						argCountDeque.Push(argCountDeque.Pop() + 1);
						while (!( arithmeticDeque.Peek() is CalcMathFunc ))
						{
							postfix.Add(arithmeticDeque.Pop());
						}

						continue;
					}

					if (arithmeticData is CalcMathFunc)
					{
						argCountDeque.Push(1);
					}
					else if (!( arithmeticData.Equals(Paropen) || arithmeticData is CalcPostfixUnaryOpr ))
					{
						while (arithmeticDeque.Count > 0 && arithmeticDeque.Peek() is CalcInfixOpr cio && cio.CompareTo((CalcInfixOpr)arithmeticData) <= 0)
						{
							postfix.Add(arithmeticDeque.Pop());
						}
					}
					arithmeticDeque.Push(arithmeticData);
				}
				catch (InvalidCastException)
				{
					if (sbForDigit.Length > 0)
					{
						throw new CalcWrongExpressionException(wrongExpressionMessage);
					}
					postfix.Add((CalcFigure)data);
				}
			}

			if (sbForDigit.Length > 0)
			{
				postfix.Add(new CalcFigure(double.Parse(sbForDigit.ToString())));
			}

			while (arithmeticDeque.Count > 0)
			{
				CalcArithmetic arithmeticData = arithmeticDeque.Pop();
				if (arithmeticData is CalcMathFunc || arithmeticData.Equals(Paropen))
				{
					throw new CalcWrongExpressionException(wrongExpressionMessage);
				}
				postfix.Add(arithmeticData);
			}
		}

		/**
		 * {@link #calculate()} and {@link #input(ch019.ex01j.calculator.CalcMathData)}
		 * @return the returned value of {@link #calculate()}
		 */
		public abstract double Ans();

		/**
		 * 
		 * @return {@link Iterator} of {@link CalcMathData}s of the last expression in order.
		 */
		public abstract IEnumerator<CalcMathData> GetEnumeratorOfExpr();

		public abstract bool CanInput(CalcData data);// => InputHelper(data, false);
		public abstract bool Input(CalcData data);// => InputHelper(data, true);
												  //protected abstract bool InputHelper(CalcData data, bool trial);
	}
}
