﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcInfixOpr : CalcArithmetic, IComparable<CalcInfixOpr>
	{
		int priority;

		public CalcInfixOpr(string text, int priority, Action operation) : base(text, operation)
		{
			this.priority = priority;
		}

		public int CompareTo(CalcInfixOpr a)
		{
			return this.priority - a.priority;
		}
	}
}
