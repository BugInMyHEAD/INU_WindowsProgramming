﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcMathFunc : CalcArithmetic
	{
		const int MaxValue = int.MaxValue;

		protected int minArg, maxArg;
		
		/**
		 * Constructs {@link CalcMathFunc} that can have {@code minArg} to {@code maxArg} arguments.
		 * @param text
		 * @param minArg has to be non-negative and less than or equal to {@code maxArg}
		 * @param maxArg has to be non-negative and greater than or equal to {@code minArg}
		 */
		public CalcMathFunc(string text, Action operation, int minArg = 1, int maxArg = 1) : base(text, operation)
		{
			if (minArg < 0)
			{
				throw new ArgumentOutOfRangeException("The argruments, 'minArg' and 'maxArg' must be non-negative.");
			}
			if (minArg > maxArg)
			{
				throw new ArgumentOutOfRangeException("'minArg' must be less than or equal to 'maxArg'");
			}

			this.minArg = minArg;
			this.maxArg = maxArg;
		}
	}
}
