﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public class CalcArithmetic : CalcMathData
	{
		//	public final boolean termBefore;
		//	public final boolean termAfter;

		protected internal Action Operate { get; }

		public CalcArithmetic(string text, Action operation) : base(text)//, boolean termBefore, boolean termAfter)
		{
			// this.termBefore = termBefore;
			// this.termAfter = termAfter;
			Operate = operation;
		}

	}
}
