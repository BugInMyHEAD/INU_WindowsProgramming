﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP112702c
{
	public abstract class CalcData
	{
		private string Text { get; }

		public CalcData(string text)
		{
			Text = text;
		}

		public override string ToString()
		{
			return Text;
		}
	}
}
