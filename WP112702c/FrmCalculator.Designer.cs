﻿namespace WP112702c
{
	partial class FrmCalculator
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.calcButtonPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.calcButton1 = new System.Windows.Forms.Button();
			this.calcButton2 = new System.Windows.Forms.Button();
			this.calcButton3 = new System.Windows.Forms.Button();
			this.calcButtonAdd = new System.Windows.Forms.Button();
			this.calcButtonMod = new System.Windows.Forms.Button();
			this.calcButton4 = new System.Windows.Forms.Button();
			this.calcButton5 = new System.Windows.Forms.Button();
			this.calcButton6 = new System.Windows.Forms.Button();
			this.calcButtonSub = new System.Windows.Forms.Button();
			this.calcButtonPow = new System.Windows.Forms.Button();
			this.calcButton7 = new System.Windows.Forms.Button();
			this.calcButton8 = new System.Windows.Forms.Button();
			this.calcButton9 = new System.Windows.Forms.Button();
			this.calcButtonMul = new System.Windows.Forms.Button();
			this.calcButtonFact = new System.Windows.Forms.Button();
			this.calcButton0 = new System.Windows.Forms.Button();
			this.calcButtonDot = new System.Windows.Forms.Button();
			this.calcButtonAns = new System.Windows.Forms.Button();
			this.calcButtonDiv = new System.Windows.Forms.Button();
			this.calcButtonSqrt = new System.Windows.Forms.Button();
			this.calcButtonDel = new System.Windows.Forms.Button();
			this.calcButtonCe = new System.Windows.Forms.Button();
			this.calcButtonClr = new System.Windows.Forms.Button();
			this.calcButtonParopen = new System.Windows.Forms.Button();
			this.calcButtonParclse = new System.Windows.Forms.Button();
			this.calcButtonPi = new System.Windows.Forms.Button();
			this.calcButtonE = new System.Windows.Forms.Button();
			this.calcDisplay = new System.Windows.Forms.TextBox();
			this.calcButtonPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// calcButtonPanel
			// 
			this.calcButtonPanel.Controls.Add(this.calcButton1);
			this.calcButtonPanel.Controls.Add(this.calcButton2);
			this.calcButtonPanel.Controls.Add(this.calcButton3);
			this.calcButtonPanel.Controls.Add(this.calcButtonAdd);
			this.calcButtonPanel.Controls.Add(this.calcButtonMod);
			this.calcButtonPanel.Controls.Add(this.calcButton4);
			this.calcButtonPanel.Controls.Add(this.calcButton5);
			this.calcButtonPanel.Controls.Add(this.calcButton6);
			this.calcButtonPanel.Controls.Add(this.calcButtonSub);
			this.calcButtonPanel.Controls.Add(this.calcButtonPow);
			this.calcButtonPanel.Controls.Add(this.calcButton7);
			this.calcButtonPanel.Controls.Add(this.calcButton8);
			this.calcButtonPanel.Controls.Add(this.calcButton9);
			this.calcButtonPanel.Controls.Add(this.calcButtonMul);
			this.calcButtonPanel.Controls.Add(this.calcButtonFact);
			this.calcButtonPanel.Controls.Add(this.calcButton0);
			this.calcButtonPanel.Controls.Add(this.calcButtonDot);
			this.calcButtonPanel.Controls.Add(this.calcButtonAns);
			this.calcButtonPanel.Controls.Add(this.calcButtonDiv);
			this.calcButtonPanel.Controls.Add(this.calcButtonSqrt);
			this.calcButtonPanel.Controls.Add(this.calcButtonDel);
			this.calcButtonPanel.Controls.Add(this.calcButtonCe);
			this.calcButtonPanel.Controls.Add(this.calcButtonClr);
			this.calcButtonPanel.Controls.Add(this.calcButtonParopen);
			this.calcButtonPanel.Controls.Add(this.calcButtonParclse);
			this.calcButtonPanel.Controls.Add(this.calcButtonPi);
			this.calcButtonPanel.Controls.Add(this.calcButtonE);
			this.calcButtonPanel.Location = new System.Drawing.Point(34, 77);
			this.calcButtonPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonPanel.Name = "calcButtonPanel";
			this.calcButtonPanel.Size = new System.Drawing.Size(334, 229);
			this.calcButtonPanel.TabIndex = 0;
			// 
			// calcButton1
			// 
			this.calcButton1.Location = new System.Drawing.Point(3, 4);
			this.calcButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton1.Name = "calcButton1";
			this.calcButton1.Size = new System.Drawing.Size(59, 29);
			this.calcButton1.TabIndex = 0;
			this.calcButton1.Text = "1";
			this.calcButton1.UseVisualStyleBackColor = true;
			this.calcButton1.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButton2
			// 
			this.calcButton2.Location = new System.Drawing.Point(68, 4);
			this.calcButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton2.Name = "calcButton2";
			this.calcButton2.Size = new System.Drawing.Size(59, 29);
			this.calcButton2.TabIndex = 1;
			this.calcButton2.Text = "2";
			this.calcButton2.UseVisualStyleBackColor = true;
			this.calcButton2.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButton3
			// 
			this.calcButton3.Location = new System.Drawing.Point(133, 4);
			this.calcButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton3.Name = "calcButton3";
			this.calcButton3.Size = new System.Drawing.Size(59, 29);
			this.calcButton3.TabIndex = 2;
			this.calcButton3.Text = "3";
			this.calcButton3.UseVisualStyleBackColor = true;
			this.calcButton3.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButtonAdd
			// 
			this.calcButtonAdd.Location = new System.Drawing.Point(198, 4);
			this.calcButtonAdd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonAdd.Name = "calcButtonAdd";
			this.calcButtonAdd.Size = new System.Drawing.Size(59, 29);
			this.calcButtonAdd.TabIndex = 3;
			this.calcButtonAdd.Text = "+";
			this.calcButtonAdd.UseVisualStyleBackColor = true;
			this.calcButtonAdd.Click += new System.EventHandler(this.CalcButtonAdd_Click);
			// 
			// calcButtonMod
			// 
			this.calcButtonMod.Location = new System.Drawing.Point(263, 4);
			this.calcButtonMod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonMod.Name = "calcButtonMod";
			this.calcButtonMod.Size = new System.Drawing.Size(59, 29);
			this.calcButtonMod.TabIndex = 3;
			this.calcButtonMod.Text = "%";
			this.calcButtonMod.UseVisualStyleBackColor = true;
			this.calcButtonMod.Click += new System.EventHandler(this.CalcButtonMod_Click);
			// 
			// calcButton4
			// 
			this.calcButton4.Location = new System.Drawing.Point(3, 41);
			this.calcButton4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton4.Name = "calcButton4";
			this.calcButton4.Size = new System.Drawing.Size(59, 29);
			this.calcButton4.TabIndex = 3;
			this.calcButton4.Text = "4";
			this.calcButton4.UseVisualStyleBackColor = true;
			this.calcButton4.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButton5
			// 
			this.calcButton5.Location = new System.Drawing.Point(68, 41);
			this.calcButton5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton5.Name = "calcButton5";
			this.calcButton5.Size = new System.Drawing.Size(59, 29);
			this.calcButton5.TabIndex = 4;
			this.calcButton5.Text = "5";
			this.calcButton5.UseVisualStyleBackColor = true;
			this.calcButton5.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButton6
			// 
			this.calcButton6.Location = new System.Drawing.Point(133, 41);
			this.calcButton6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton6.Name = "calcButton6";
			this.calcButton6.Size = new System.Drawing.Size(59, 29);
			this.calcButton6.TabIndex = 5;
			this.calcButton6.Text = "6";
			this.calcButton6.UseVisualStyleBackColor = true;
			this.calcButton6.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButtonSub
			// 
			this.calcButtonSub.Location = new System.Drawing.Point(198, 41);
			this.calcButtonSub.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonSub.Name = "calcButtonSub";
			this.calcButtonSub.Size = new System.Drawing.Size(59, 29);
			this.calcButtonSub.TabIndex = 6;
			this.calcButtonSub.Text = "-";
			this.calcButtonSub.UseVisualStyleBackColor = true;
			this.calcButtonSub.Click += new System.EventHandler(this.CalcButtonSub_Click);
			// 
			// calcButtonPow
			// 
			this.calcButtonPow.Location = new System.Drawing.Point(263, 41);
			this.calcButtonPow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonPow.Name = "calcButtonPow";
			this.calcButtonPow.Size = new System.Drawing.Size(59, 29);
			this.calcButtonPow.TabIndex = 6;
			this.calcButtonPow.Text = "^";
			this.calcButtonPow.UseVisualStyleBackColor = true;
			this.calcButtonPow.Click += new System.EventHandler(this.CalcButtonPow_Click);
			// 
			// calcButton7
			// 
			this.calcButton7.Location = new System.Drawing.Point(3, 78);
			this.calcButton7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton7.Name = "calcButton7";
			this.calcButton7.Size = new System.Drawing.Size(59, 29);
			this.calcButton7.TabIndex = 6;
			this.calcButton7.Text = "7";
			this.calcButton7.UseVisualStyleBackColor = true;
			this.calcButton7.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButton8
			// 
			this.calcButton8.Location = new System.Drawing.Point(68, 78);
			this.calcButton8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton8.Name = "calcButton8";
			this.calcButton8.Size = new System.Drawing.Size(59, 29);
			this.calcButton8.TabIndex = 7;
			this.calcButton8.Text = "8";
			this.calcButton8.UseVisualStyleBackColor = true;
			this.calcButton8.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButton9
			// 
			this.calcButton9.Location = new System.Drawing.Point(133, 78);
			this.calcButton9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton9.Name = "calcButton9";
			this.calcButton9.Size = new System.Drawing.Size(59, 29);
			this.calcButton9.TabIndex = 7;
			this.calcButton9.Text = "9";
			this.calcButton9.UseVisualStyleBackColor = true;
			this.calcButton9.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButtonMul
			// 
			this.calcButtonMul.Location = new System.Drawing.Point(198, 78);
			this.calcButtonMul.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonMul.Name = "calcButtonMul";
			this.calcButtonMul.Size = new System.Drawing.Size(59, 29);
			this.calcButtonMul.TabIndex = 7;
			this.calcButtonMul.Text = "*";
			this.calcButtonMul.UseVisualStyleBackColor = true;
			this.calcButtonMul.Click += new System.EventHandler(this.CalcButtonMul_Click);
			// 
			// calcButtonFact
			// 
			this.calcButtonFact.Location = new System.Drawing.Point(263, 78);
			this.calcButtonFact.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonFact.Name = "calcButtonFact";
			this.calcButtonFact.Size = new System.Drawing.Size(59, 29);
			this.calcButtonFact.TabIndex = 7;
			this.calcButtonFact.Text = "!";
			this.calcButtonFact.UseVisualStyleBackColor = true;
			this.calcButtonFact.Click += new System.EventHandler(this.CalcButtonFact_Click);
			// 
			// calcButton0
			// 
			this.calcButton0.Location = new System.Drawing.Point(3, 115);
			this.calcButton0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButton0.Name = "calcButton0";
			this.calcButton0.Size = new System.Drawing.Size(59, 29);
			this.calcButton0.TabIndex = 7;
			this.calcButton0.Text = "0";
			this.calcButton0.UseVisualStyleBackColor = true;
			this.calcButton0.Click += new System.EventHandler(this.CalcDigitButton_Click);
			// 
			// calcButtonDot
			// 
			this.calcButtonDot.Location = new System.Drawing.Point(68, 115);
			this.calcButtonDot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonDot.Name = "calcButtonDot";
			this.calcButtonDot.Size = new System.Drawing.Size(59, 29);
			this.calcButtonDot.TabIndex = 7;
			this.calcButtonDot.Text = ".";
			this.calcButtonDot.UseVisualStyleBackColor = true;
			this.calcButtonDot.Click += new System.EventHandler(this.CalcButtonDot_Click);
			// 
			// calcButtonAns
			// 
			this.calcButtonAns.Location = new System.Drawing.Point(133, 115);
			this.calcButtonAns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonAns.Name = "calcButtonAns";
			this.calcButtonAns.Size = new System.Drawing.Size(59, 29);
			this.calcButtonAns.TabIndex = 7;
			this.calcButtonAns.Text = "=";
			this.calcButtonAns.UseVisualStyleBackColor = true;
			this.calcButtonAns.Click += new System.EventHandler(this.CalcButtonAns_Click);
			// 
			// calcButtonDiv
			// 
			this.calcButtonDiv.Location = new System.Drawing.Point(198, 115);
			this.calcButtonDiv.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonDiv.Name = "calcButtonDiv";
			this.calcButtonDiv.Size = new System.Drawing.Size(59, 29);
			this.calcButtonDiv.TabIndex = 7;
			this.calcButtonDiv.Text = "/";
			this.calcButtonDiv.UseVisualStyleBackColor = true;
			this.calcButtonDiv.Click += new System.EventHandler(this.CalcButtonDiv_Click);
			// 
			// calcButtonSqrt
			// 
			this.calcButtonSqrt.Location = new System.Drawing.Point(263, 115);
			this.calcButtonSqrt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonSqrt.Name = "calcButtonSqrt";
			this.calcButtonSqrt.Size = new System.Drawing.Size(59, 29);
			this.calcButtonSqrt.TabIndex = 7;
			this.calcButtonSqrt.Text = "√";
			this.calcButtonSqrt.UseVisualStyleBackColor = true;
			this.calcButtonSqrt.Click += new System.EventHandler(this.CalcButtonSqrt_Click);
			// 
			// calcButtonDel
			// 
			this.calcButtonDel.Location = new System.Drawing.Point(3, 152);
			this.calcButtonDel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonDel.Name = "calcButtonDel";
			this.calcButtonDel.Size = new System.Drawing.Size(59, 29);
			this.calcButtonDel.TabIndex = 7;
			this.calcButtonDel.Text = "DEL";
			this.calcButtonDel.UseVisualStyleBackColor = true;
			this.calcButtonDel.Click += new System.EventHandler(this.CalcButtonDel_Click);
			// 
			// calcButtonCe
			// 
			this.calcButtonCe.Location = new System.Drawing.Point(68, 152);
			this.calcButtonCe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonCe.Name = "calcButtonCe";
			this.calcButtonCe.Size = new System.Drawing.Size(59, 29);
			this.calcButtonCe.TabIndex = 7;
			this.calcButtonCe.Text = "CE";
			this.calcButtonCe.UseVisualStyleBackColor = true;
			this.calcButtonCe.Click += new System.EventHandler(this.CalcButtonCe_Click);
			// 
			// calcButtonClr
			// 
			this.calcButtonClr.Location = new System.Drawing.Point(133, 152);
			this.calcButtonClr.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonClr.Name = "calcButtonClr";
			this.calcButtonClr.Size = new System.Drawing.Size(59, 29);
			this.calcButtonClr.TabIndex = 7;
			this.calcButtonClr.Text = "C";
			this.calcButtonClr.UseVisualStyleBackColor = true;
			this.calcButtonClr.Click += new System.EventHandler(this.CalcButtonClr_Click);
			// 
			// calcButtonParopen
			// 
			this.calcButtonParopen.Location = new System.Drawing.Point(198, 152);
			this.calcButtonParopen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonParopen.Name = "calcButtonParopen";
			this.calcButtonParopen.Size = new System.Drawing.Size(59, 29);
			this.calcButtonParopen.TabIndex = 7;
			this.calcButtonParopen.Text = "(";
			this.calcButtonParopen.UseVisualStyleBackColor = true;
			this.calcButtonParopen.Click += new System.EventHandler(this.CalcButtonParopen_Click);
			// 
			// calcButtonParclse
			// 
			this.calcButtonParclse.Location = new System.Drawing.Point(263, 152);
			this.calcButtonParclse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonParclse.Name = "calcButtonParclse";
			this.calcButtonParclse.Size = new System.Drawing.Size(59, 29);
			this.calcButtonParclse.TabIndex = 7;
			this.calcButtonParclse.Text = ")";
			this.calcButtonParclse.UseVisualStyleBackColor = true;
			this.calcButtonParclse.Click += new System.EventHandler(this.CalcButtonParclse_Click);
			// 
			// calcButtonPi
			// 
			this.calcButtonPi.Location = new System.Drawing.Point(3, 189);
			this.calcButtonPi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonPi.Name = "calcButtonPi";
			this.calcButtonPi.Size = new System.Drawing.Size(59, 29);
			this.calcButtonPi.TabIndex = 7;
			this.calcButtonPi.Text = "π";
			this.calcButtonPi.UseVisualStyleBackColor = true;
			this.calcButtonPi.Click += new System.EventHandler(this.CalcButtonPi_Click);
			// 
			// calcButtonE
			// 
			this.calcButtonE.Location = new System.Drawing.Point(68, 189);
			this.calcButtonE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcButtonE.Name = "calcButtonE";
			this.calcButtonE.Size = new System.Drawing.Size(59, 29);
			this.calcButtonE.TabIndex = 7;
			this.calcButtonE.Text = "e";
			this.calcButtonE.UseVisualStyleBackColor = true;
			this.calcButtonE.Click += new System.EventHandler(this.CalcButtonE_Click);
			// 
			// calcDisplay
			// 
			this.calcDisplay.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.calcDisplay.Location = new System.Drawing.Point(12, 13);
			this.calcDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.calcDisplay.Name = "calcDisplay";
			this.calcDisplay.ReadOnly = true;
			this.calcDisplay.Size = new System.Drawing.Size(376, 25);
			this.calcDisplay.TabIndex = 1;
			// 
			// FrmCalculator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(419, 338);
			this.Controls.Add(this.calcDisplay);
			this.Controls.Add(this.calcButtonPanel);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "FrmCalculator";
			this.Text = "김건우 계산기";
			this.calcButtonPanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel calcButtonPanel;
		private System.Windows.Forms.Button calcButton1;
		private System.Windows.Forms.Button calcButton2;
		private System.Windows.Forms.Button calcButton3;
		private System.Windows.Forms.Button calcButtonAdd;
		private System.Windows.Forms.Button calcButton4;
		private System.Windows.Forms.Button calcButton5;
		private System.Windows.Forms.Button calcButton6;
		private System.Windows.Forms.Button calcButtonSub;
		private System.Windows.Forms.Button calcButton7;
		private System.Windows.Forms.Button calcButton8;
		private System.Windows.Forms.Button calcButton9;
		private System.Windows.Forms.Button calcButtonMul;
		private System.Windows.Forms.Button calcButton0;
		private System.Windows.Forms.Button calcButtonClr;
		private System.Windows.Forms.Button calcButtonAns;
		private System.Windows.Forms.Button calcButtonDiv;
		internal System.Windows.Forms.TextBox calcDisplay;
		private System.Windows.Forms.Button calcButtonMod;
		private System.Windows.Forms.Button calcButtonPow;
		private System.Windows.Forms.Button calcButtonFact;
		private System.Windows.Forms.Button calcButtonSqrt;
		private System.Windows.Forms.Button calcButtonDel;
		private System.Windows.Forms.Button calcButtonCe;
		private System.Windows.Forms.Button calcButtonDot;
		private System.Windows.Forms.Button calcButtonPi;
		private System.Windows.Forms.Button calcButtonParopen;
		private System.Windows.Forms.Button calcButtonParclse;
		private System.Windows.Forms.Button calcButtonE;
	}
}

