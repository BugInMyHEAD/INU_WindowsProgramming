﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WP112202c
{
	public partial class FrmRandomQuiz : Form
	{
		GroupBox[] gb = new GroupBox[2];
		RadioButton[,] bogi = new RadioButton[2, 5];
		//Label[] Quiz = new Label[2];
		Label lbScore = new Label();
		Button button = new Button();
		int score = 0;

		public FrmRandomQuiz()
		{
			InitializeComponent();

			string aa = "[문제 ";   // 1]  은 소스에서 추가 해야 함
			string[,] Question = new string[2, 6]
			{
				{ "다음중 1+1 = ?" ,"일", "이", "삼", "사", "이" },
				{ "인천에 있는 산은 ?", "한라산", "백두산", "계양산", "금강산", "계양산"}
			};

			#region
			lbScore.Name = "lblScore";
			lbScore.Text = "Score : ";
			lbScore.Size = new Size(90, 30);
			lbScore.AutoSize = true;
			lbScore.Location = new Point(30, 380);
			Controls.Add(lbScore);
			#endregion

			#region
			button.Name = "btnSubmit";
			button.Text = "채점하기";
			button.Size = new Size(90, 30);
			button.AutoSize = true;
			button.Location = new Point(180, 390);
			button.Click += Button_Click;
			#endregion

			#region
			for (var i2 = 0; i2 < gb.Length; i2++)
			{
				gb[i2] = new GroupBox
				{
					AutoSize = true,
					Location = new Point(10, 10 + ( i2 * 150 )),
					Size = new Size(350, 120),
					TabIndex = 0,
					TabStop = false,
					Text = aa + ( i2 + 1 ) + "]   " + Question[i2, 0]
				};
				Controls.Add(gb[i2]);

				for (var i4 = 0; i4 < 5; i4++)
				{
					bogi[i2, i4] = new RadioButton
					{
						Location = new Point(30, 30 + i4 * 20),
						Size = new Size(100, 20),
						Text = Question[i2, i4 + 1],
					};
					if (i4 == 4)
					{
						bogi[i2, 4].Visible = false;
					}
					gb[i2].Controls.Add(bogi[i2, i4]);
				}
			}
			#endregion

			AutoSize = true;
		}

		private void Button_Click(object sender, EventArgs e)
		{

		}

	}
}
