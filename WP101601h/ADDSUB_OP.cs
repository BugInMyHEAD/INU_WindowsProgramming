﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP101601h
{
	public class ADDSUB_OP
	{
		public int Add(int num1, int num2) => num1 + num2;

		public int Sub(int num1, int num2) => num1 - num2;
	}
}
