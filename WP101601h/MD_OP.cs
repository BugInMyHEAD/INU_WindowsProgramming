﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M_D
{
	public static class MD_OP
	{
		public static int Mult(int num1, int num2) => num1 * num2;

		public static int Divide(int num1, int num2) => num1 / num2;
	}
}
