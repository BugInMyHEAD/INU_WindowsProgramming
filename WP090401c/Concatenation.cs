﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP090401c
{
    class Concatenation
    {
        static void Main(string[] args)
        {
            string s1, s2;
            Console.WriteLine("문자열 s1 입력 : ");
            s1 = Console.ReadLine();
            Console.WriteLine("문자열 s2 입력 : ");
            s2 = Console.ReadLine();
            Console.WriteLine(s1 + s2);
        }
    }
}
