﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP091801c
{
	class Exam
	{
		static void Main(string[] args)
		{
			for (; ; Console.WriteLine())
			{
				Console.Write("점수 입력 : ");
				var intFromUser = int.Parse(Console.ReadLine());

				if (-999 == intFromUser)
					break;

				Console.Write(" ");

				Grade grade;
				try
				{
					grade = GetGrade(intFromUser);
				}
				catch (ArgumentOutOfRangeException)
				{
					Console.WriteLine("점수 범위 초과 됨");
					continue;
				}

				Console.WriteLine("학점은 " + grade);
			}
		}

		public static Grade GetGrade(int score)
		{
			if(score < 0)
			{
				throw new ArgumentOutOfRangeException();
			}

			if (score > 100)
			{
				throw new ArgumentOutOfRangeException();
			}

			Grade retVal;
			int dividedScore = score / 10;
			switch (dividedScore)
			{
				case 6:
				case 7:
				case 8:
				case 9:
					retVal = (Grade)dividedScore;
					break;
				case 10:
					retVal = Grade.A;
					break;
				default:
					retVal = Grade.F;
					break;
			}

			return retVal;
		}

		public enum Grade { F, D=6, C, B, A };
	}
}
