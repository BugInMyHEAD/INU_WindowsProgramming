﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WP092503c;

namespace WP112701d
{
	public partial class FrmIuRandomTest : Form
	{
		Question[] question =
			{
				"우리나라 수도는?",
				"인천대학교 위치는?",
				"인천의 산은?",
				"다음 중에서 아이유가 아닌 것은?",
				"아래 사진은 누구인가?"
			};

		Label lblScoreFigure = new Label();

		int[] pickedQuestionIdx;

		public FrmIuRandomTest()
		{
			InitializeComponent();

			ButtonBase[] questionButtons;
			int[] pickedButtonIdx;

			// 0
			questionButtons = new QuestionRadioButton[]
				{
					new QuestionRadioButton("서울", true),
					new QuestionRadioButton("인천"),
					new QuestionRadioButton("부산"),
					new QuestionRadioButton("광주"),
				};
			pickedButtonIdx = Lottery.Pick(questionButtons.Length, 0, questionButtons.Length);
			{
				int i1 = 0;

				foreach (var var2 in pickedButtonIdx)
				{
					question[0].Grp.Controls.Add(questionButtons[var2]);
					questionButtons[var2].Location = new Point(i1 * 70 + 20, 30);
					questionButtons[var2].AutoSize = true;

					i1++;
				}
			}
			question[0].Grp.Size = new Size(400, 60);

			// 1
			questionButtons = new QuestionCheckBox[]
				{
					new QuestionCheckBox("인천", true),
					new QuestionCheckBox("송도", true),
					new QuestionCheckBox("서울"),
					new QuestionCheckBox("청주"),
				};
			pickedButtonIdx = Lottery.Pick(questionButtons.Length, 0, questionButtons.Length);
			{
				int i1 = 0;

				foreach (var var2 in pickedButtonIdx)
				{
					question[1].Grp.Controls.Add(questionButtons[var2]);
					questionButtons[var2].Location = new Point(i1 * 70 + 20, 30);
					questionButtons[var2].AutoSize = true;

					i1++;
				}
			}
			question[1].Grp.Size = new Size(400, 60);

			// 2
			questionButtons = new QuestionRadioButton[]
				{
					new QuestionRadioButton("계양산", true),
					new QuestionRadioButton("백두산"),
					new QuestionRadioButton("한라산"),
					new QuestionRadioButton("지리산"),
				};
			pickedButtonIdx = Lottery.Pick(questionButtons.Length, 0, questionButtons.Length);
			{
				int i1 = 0;

				foreach (var var2 in pickedButtonIdx)
				{
					question[2].Grp.Controls.Add(questionButtons[var2]);
					questionButtons[var2].Location = new Point(i1 * 70 + 20, 30);
					questionButtons[var2].AutoSize = true;

					i1++;
				}
			}
			question[2].Grp.Size = new Size(400, 60);

			// 3
			questionButtons = new QuestionRadioButton[]
				{
					new QuestionRadioButton("", true) { Image = Properties.Resources.Pic4_1 },
					new QuestionRadioButton { Image = Properties.Resources.Pic4_2 },
					new QuestionRadioButton { Image = Properties.Resources.Pic4_3 },
					new QuestionRadioButton { Image = Properties.Resources.Pic4_4 },
				};
			pickedButtonIdx = Lottery.Pick(questionButtons.Length, 0, questionButtons.Length);
			{
				int i1 = 0;

				foreach (var var2 in pickedButtonIdx)
				{
					question[3].Grp.Controls.Add(questionButtons[var2]);
					questionButtons[var2].Location = new Point((i1 + 1) % 2 * 160 + 20, i1 / 2 * 160 + 30);
					questionButtons[var2].AutoSize = true;
					questionButtons[var2].ImageAlign = ContentAlignment.MiddleRight;

					i1++;
				}
			}
			question[3].Grp.Size = new Size(400, 350);

			// 4
			questionButtons = new QuestionRadioButton[]
				{
					new QuestionRadioButton("아이유", true),
					new QuestionRadioButton("아이돌"),
					new QuestionRadioButton("아이큐"),
					new QuestionRadioButton("아이비"),
				};
			pickedButtonIdx = Lottery.Pick(questionButtons.Length, 0, questionButtons.Length);
			{
				int i1 = 0;

				foreach (var var2 in pickedButtonIdx)
				{
					question[4].Grp.Controls.Add(questionButtons[var2]);
					questionButtons[var2].Location = new Point(170 + 20, i1 * 20 + 50);
					questionButtons[var2].AutoSize = true;

					i1++;
				}
			}
			var imgBtn = new Button
			{
				Image = Properties.Resources.Pic5_5,
				Location = new Point(20, 40),
				AutoSize = true
			};
			question[4].Grp.Controls.Add(imgBtn);
			question[4].Grp.Size = new Size(400, 200);

			pickedQuestionIdx = Lottery.Pick(3, 0, question.Length);
			int nextY = 20;
			{
				int i1 = 1;
				foreach (var var2 in pickedQuestionIdx)
				{
					question[var2].Grp.Text = i1.ToString() + ". " + question[var2].Text;
					question[var2].Grp.Location = new Point(20, nextY);
					Controls.Add(question[var2].Grp);
					nextY = question[var2].Grp.Location.Y + question[var2].Grp.Height + 20;
					i1++;
				}
			}

			// bottom
			var btnGrading = new Button
			{
				Text = "채점하기",
				Location = new Point(100, nextY),
			};
			btnGrading.Click += new System.EventHandler(BtnGrading_Click);
			var lblScoreLable = new Label
			{
				Text = "점수 : ",
				Location = new Point(300, nextY),
			};
			lblScoreFigure.Location = new Point(400, nextY);
			Controls.Add(btnGrading);
			Controls.Add(lblScoreLable);
			Controls.Add(lblScoreFigure);
		}

		private void BtnGrading_Click(object sender, EventArgs e)
		{
			int score = 0;

			foreach (var var2 in pickedQuestionIdx)
			{
				int i3 = -1;
				bool i3set = false;

				foreach (var var4 in question[var2].Grp.Controls.OfType<IQuestionButton>())
				{
					if (var4 is QuestionRadioButton)
					{
						if (!i3set)
						{
							i3 = 0;
							i3set = true;
						}
						
						if (var4.IsCorrect && var4.Checked)
						{
							i3 = 1;

							break;
						}
					}
					else if (var4 is QuestionCheckBox)
					{
						if (!i3set)
						{
							i3 = 1;
							i3set = true;
						}

						if (var4.IsCorrect ^ var4.Checked)
						{
							i3 = 0;

							break;
						}
					}
					else
					{
						throw new ApplicationException();
					}
				}
				
				if (i3 < 0)
				{
					throw new ApplicationException();
				}
				score += i3;
			}

			lblScoreFigure.Text = score.ToString();
		}
	}

	class Question
	{
		internal GroupBox Grp { get; }

		public string Text { get; set; }

		public Question(string text = "")
		{
			Text = text;
			Grp = new GroupBox();
		}

		public static implicit operator Question(string text)
		{
			return new Question(text);
		}
	}

	interface IQuestionButton
	{
		bool IsCorrect { get; }
		bool Checked { get; }
	}

	class QuestionRadioButton : RadioButton, IQuestionButton
	{
		public bool IsCorrect { get; }

		public QuestionRadioButton(string text = "", bool isCorrect = false)
		{
			Text = text;
			IsCorrect = isCorrect;
		}
	}

	class QuestionCheckBox : CheckBox, IQuestionButton
	{
		public bool IsCorrect { get; }

		public QuestionCheckBox(string text = "", bool isCorrect = false)
		{
			Text = text;
			IsCorrect = isCorrect;
		}
	}
}