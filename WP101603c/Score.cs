﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP101603c
{
	class Score
	{
		static void Main(string[] args)
		{
			string[] colStr = { "학번", "이름", "국어", "수학", "총점" };
			string guideStr = " 입력 : ";

			var myScore = new Score();

			Console.Write($"{colStr[0]}{guideStr}");
			myScore.Id = Console.ReadLine();
			Console.Write($"{colStr[1]}{guideStr}");
			myScore.Name = Console.ReadLine();
			Console.Write($"{colStr[2]}{guideStr}");
			myScore.Kor = int.Parse(Console.ReadLine());
			Console.Write($"{colStr[3]}{guideStr}");
			myScore.Mat = int.Parse(Console.ReadLine());

			Console.WriteLine("==================================");
			const int colLen = 6;
			foreach (var v2 in colStr)
			{
				///Console.Write($"{v2,colLen-2}");
				//dsafjkdas;f
				
			}
			Console.WriteLine();
			Console.WriteLine($"{myScore.Id,colLen}{myScore.Name,colLen}{myScore.Kor,colLen}{myScore.Mat,colLen}{myScore.Tot,colLen}");
		}

		public string Id { get; set; }
		public string Name { get; set; }
		public int Kor { get; set; }
		public int Mat { get; set; }

		public int Tot => Kor + Mat;
	}
}
