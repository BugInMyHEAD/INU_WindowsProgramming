﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WP102502c
{
	class 주사위
	{
		static Random random = new Random();

		static void Main(string[] args)
		{
			Console.WriteLine("                      주사위 던지기\n");

			int[] diceCount = new int[7];

			for (var i2 = 0; i2 < 5; i2++)
			{
				for (var i4 = 0; i4 < 6; i4++)
				{
					var i5 = random.Next(1, 7);
					diceCount[i5]++;
					Console.Write(i5 + "          ");
				}
				Console.WriteLine();
			}
			Console.WriteLine();

			int maxCount = int.MinValue;
			for (var i2 = 1; i2 < diceCount.Length; i2++)
			{
				Console.WriteLine($"{i2} 선택한 횟수는 {diceCount[i2]} 번 입니다.");
				maxCount = maxCount < diceCount[i2] ? diceCount[i2] : maxCount;
			}
			Console.WriteLine();

			Console.Write("가장 많이 발생된 수는 ");
			for (var i2 = 1; i2 < diceCount.Length; i2++)
			{
				if (diceCount[i2] == maxCount) Console.Write(i2 + " ");
			}
			Console.WriteLine($"로 {maxCount} 번 입니다.");
		}
	}
}
